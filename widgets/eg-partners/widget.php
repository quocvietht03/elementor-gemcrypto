<?php
namespace ElementorGemcrypto\Widgets\Partners;


use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;



if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Partners extends Widget_Base {

	public function get_name() {
		return 'eg-partners';
	}

	public function get_title() {
		return __( 'EG Partners', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-partners', 'eg-show-more' ];
	}

	public function get_script_depends() {
		return ['eg-show-more'];
	}

  protected function register_layout_section_controls() {
    $this->start_controls_section(
      'image_section',
      [
        'label' => __( 'Image List', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_responsive_control(
      'columns',
      [
        'label' => __( 'Columns', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => '4',
        'tablet_default' => '3',
        'mobile_default' => '2',
        'options' => [
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
          '5' => '5',
          '6' => '6',
        ],
        'prefix_class' => 'elementor-grid%s-',
      ]
    );

    $repeater = new Repeater();

    $repeater->add_control(
			'list_image', [
				'label' => __( 'Thumbnail', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

    $repeater->add_control(
      'list_link',
      [
        'label' => esc_html__( 'Link', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::URL,
        'dynamic' => [
          'active' => false,
        ],
        'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'list',
      [
        'label' => esc_html__( 'Items', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_link' => '',
          ],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_link' => '',
          ],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_link' => '',
          ],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_link' => '',
          ],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_link' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_link' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_link' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_link' => '',
					],

        ],
        'title_field' => '',
      ]
    );

    $this->end_controls_section();

    $this->start_controls_section(
      'button_section',
      [
        'label' => __( 'Load More', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_control(
      'show_more',
      [
        'label' => __( 'Show More', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SWITCHER,
				'description' => __( 'Number items in list is greater than init show number.', 'elementor-gemcrypto' ),
        'default' => '',
      ]
    );

		$this->add_control(
			'init_show',
			[
				'label' => __( 'Init Show', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'default' => 4,
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

    $this->add_control(
      'init_load', [
        'label' => __( 'Init Load', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'min' => 1,
        'default' => 4,
        'condition' => [
          'show_more' => 'yes'
        ],
      ]
    );

		$this->add_control(
			'show_more_text', [
				'label' => __( 'Show More Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Show More',
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

		$this->add_control(
			'show_less_text', [
				'label' => __( 'Show Less Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Show Less',
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

    $this->end_controls_section();

  }

  protected function register_design_latyout_section_controls() {
    $this->start_controls_section(
      'section_design_layout',
      [
        'label' => __( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_control(
      'column_gap',
      [
        'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_control(
      'row_gap',
      [
        'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_control(
      'button_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'condition' => [
          'show_more' => 'yes',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-show-more' => 'margin-top: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'alignment',
      [
        'label' => __( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
        'options' => [
          'left' => [
            'title' => __( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => __( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => __( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'condition' => [
          'show_more' => 'yes',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-show-more' => 'text-align: {{VALUE}};',
        ],
      ]
    );

    $this->end_controls_section();

  }


  protected function register_design_style_image_box_section_controls() {
    $this->start_controls_section(
			'image_list_style_section',
			[
				'label' => esc_html__( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'image_background_color',
			[
				'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-partner-item .eg-thumbnail' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'image_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .eg-partner-item .eg-thumbnail' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

    $this->add_control(
      'image_border_radius',
      [
        'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => [ 'px', '%' ],
        'selectors' => [
          '{{WRAPPER}} .eg-partner-item .eg-thumbnail' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'image_box_shadow',
				'selector' => '{{WRAPPER}} .eg-partner-item .eg-thumbnail',
			]
		);

    $this->start_controls_tabs( 'thumbnail_effects_tabs' );

    $this->start_controls_tab( 'normal',
      [
        'label' => __( 'Normal', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_group_control(
      Group_Control_Css_Filter::get_type(),
      [
        'name' => 'thumbnail_filters',
        'selector' => '{{WRAPPER}} .eg-partner-item .eg-thumbnail img',
      ]
    );

    $this->end_controls_tab();

    $this->start_controls_tab( 'hover',
      [
        'label' => __( 'Hover', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_group_control(
      Group_Control_Css_Filter::get_type(),
      [
        'name' => 'thumbnail_hover_filters',
        'selector' => '{{WRAPPER}} .eg-partner-item:hover .eg-thumbnail img',
      ]
    );

    $this->end_controls_tab();

    $this->end_controls_tabs();

    $this->end_controls_section();

  }

	protected function register_design_style_button_section_controls()
	{
		$this->start_controls_section(
			'section_design_show_more',
			[
				'label' => esc_html__( 'Show More', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_more' => 'yes',
				],
			]
		);

		$this->add_control(
			'show_more_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-show-more--btn-view-',
			]
		);

		$this->add_control(
			'show_more_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'show_more_border',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'show_more_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'show_more_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-show-more a',
			]
		);

		$this->start_controls_tabs( 'tabs_show_more' );

		$this->start_controls_tab(
			'tab_show_more_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_show_more_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

  protected function register_controls() {
    $this->register_layout_section_controls();
    $this->register_design_latyout_section_controls();
    $this->register_design_style_image_box_section_controls();
		$this->register_design_style_button_section_controls();
  }

  public function render_element_header() {
		$settings = $this->get_settings_for_display();
		$classes = 'eg-partners';

		if( $settings['show_more'] === 'yes' ) {
			$classes .= ' has-show-more';
		}

    $this->add_render_attribute( 'wrapper', 'class', $classes );

	  ?>
      <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
    <?php
  }

  public function render_element_footer() {
    ?>
      </div>
    <?php
  }

	public function render_element_show_more() {
		$settings = $this->get_settings_for_display();
		if( $settings['show_more'] !== 'yes' || count( $settings['list'] ) <= $settings['init_show'] ) {
			return;
		}

		echo '<div class="eg-show-more" data-show="' . $settings['init_show'] . '" data-load="' . $settings['init_load'] . '" >
						<a class="eg-show-more-btn" href="#">' . $settings['show_more_text'] . '</a>
						<a class="eg-show-less-btn" href="#">' . $settings['show_less_text'] . '</a>
					</div>';
	}

  protected function render() {
    $settings = $this->get_settings_for_display();

    $this->render_element_header();

    ?>
    <div class="elementor-grid eg-list-partners" >
			<?php
        if( !empty( $settings['list'] ) ) {
          foreach ( $settings['list'] as $key => $value ) {
            $attachment = wp_get_attachment_image_src( $value['list_image']['id'], 'full' );
            $thumbnail = !empty( $attachment ) ? $attachment[0] : $value['list_image']['url'];

            $link_key = 'link_' . $key;
            $this->add_link_attributes( $link_key, $value['list_link'] );

            echo '<div class="elementor-item eg-partner-item">';

              echo '<div class="eg-thumbnail">';
                if( !empty( $value['list_link']['url'] ) ) {
                  echo '<a ' . $this->get_render_attribute_string( $link_key ) . ' ><img src=" ' . esc_url( $thumbnail ) . ' " alt=""></a>';
                } else {
                  echo '<img src=" ' . esc_url( $thumbnail ) . ' " alt="">';
                }
              echo '</div>';

            echo '</div>';
          }
        }
      ?>
		</div>
    <?php

		$this->render_element_show_more();

    $this->render_element_footer();

  }

  protected function content_template() {

  }

}




 ?>
