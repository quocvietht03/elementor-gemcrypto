<?php
namespace ElementorGemcrypto\Widgets\Icon_Box;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Icon_Box extends Widget_Base {

	public function get_name() {
		return 'eg-icon-box';
	}

	public function get_title() {
		return __( 'EG Icon Box', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-icon-box' ];
	}

	public function get_script_depends() {
		return [];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fa fa-star',
					'library' => 'fa-solid',
				],
			]
		);

		$this->add_control(
			'icon_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-icon-box--icon-view-',
			]
		);

		$this->add_control(
			'icon_shape',
			[
				'label' => __( 'Shape', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'circle',
				'options' => [
					'circle' => __( 'Circle', 'elementor-gemcrypto' ),
					'radius' => __( 'Radius', 'elementor-gemcrypto' ),
					'square' => __( 'Square', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_view!' => '',
				],
				'prefix_class' => 'eg-icon-box--icon-shape-',
			]
		);

		$this->add_control(
			'icon_position',
			[
				'label' => __( 'Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => 'top',
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-left',
					],
					'top' => [
						'title' => __( 'Top', 'elementor-gemcrypto' ),
						'icon' => 'eicon-v-align-top',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'eg-icon-box--icon-position-',
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'This is the heading', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'desc',
			[
				'label' => __( 'Description', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'custom_link',
			[
				'label' => __( 'Custom Link', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'None', 'elementor-gemcrypto' ),
					'title_url' => __( 'Title Url', 'elementor-gemcrypto' ),
					'extra_link' => __( 'Extra Link', 'elementor-gemcrypto' ),
				]
			]
		);

		$this->add_control(
			'link_text',
			[
				'label' => __( 'Link Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Click Here', 'elementor-gemcrypto' ),
				'condition' => [
					'custom_link' => 'extra_link',
				],
			]
		);

		$this->add_control(
			'link_url',
			[
				'label' => __( 'Link Url', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::URL,
				'label_block' => true,
				'placeholder' => __( 'https://your-link.com', 'elementor-gemcrypto' ),
				'show_external' => true,
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
				'condition' => [
					'custom_link!' => '',
				],
			]
		);

		$this->add_control(
			'link_selected_icon',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'skin' => 'inline',
				'label_block' => false,
				'condition' => [
					'custom_link' => 'extra_link',
				],
			]
		);

		$this->add_control(
			'link_icon_align',
			[
				'label' => esc_html__( 'Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
					'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'custom_link' => 'extra_link',
					'link_selected_icon[value]!' => '',
				],
			]
		);

		$this->add_control(
			'link_icon_indent',
			[
				'label' => esc_html__( 'Icon Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'condition' => [
					'custom_link' => 'extra_link',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-link-btn--icon-after i,
					 {{WRAPPER}} .eg-link-btn--icon-after svg' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-link-btn--icon-before i,
					 {{WRAPPER}} .eg-link-btn--icon-before svg' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_layout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'condition' => [
					'icon_position' => ['', 'top'],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'vertical_alignment',
			[
				'label' => __( 'Vertical Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'top',
				'options' => [
					'top' => __( 'Top', 'elementor-gemcrypto' ),
					'middle' => __( 'Middle', 'elementor-gemcrypto' ),
					'bottom' => __( 'Bottom', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_position!' => ['', 'top'],
				],
				'prefix_class' => 'eg-icon-box--vertical-align-',
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_icon_section_controls() {
		$this->start_controls_section(
			'section_design_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'icon_indent',
			[
				'label' => __( 'Icon Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-position-top .eg-icon-wrap' => 'margin-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.eg-icon-box--icon-position-left .eg-icon-wrap' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.eg-icon-box--icon-position-right .eg-icon-wrap' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_size',
			[
				'label' => __( 'Icon Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon i' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-icon' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'icon_view!' => '',
				],
			]
		);

		$this->add_control(
			'icon_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon,
					 {{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_icon' );

		$this->start_controls_tab(
			'tab_icon_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'icon_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_icon_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon-box:hover .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked:hover .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed:hover .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked:hover .eg-icon' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed:hover .eg-icon' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'icon_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_spacing',
			[
				'label' => __( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'custom_link' => 'title_url',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_title',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-title',
			]
		);

		$this->add_control(
			'heading_desc_style',
			[
				'label' => __( 'Description', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-desc' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_desc',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-desc',
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_custom_link_section_controls() {
		$this->start_controls_section(
			'section_design_custom_link',
			[
				'label' => __( 'Custom Link', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'custom_link' => 'extra_link',
				],
			]
		);

		$this->add_control(
			'extra_link_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'custom_link' => 'extra_link',
				],
				'prefix_class' => 'eg-icon-box--extra-link-view-',
			]
		);

		$this->add_control(
			'extra_link_spacing',
			[
				'label' => __( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'custom_link' => 'extra_link',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-link-btn' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'extra_link_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-stacked .eg-icon-box .eg-link-btn,
					 {{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'extra_link_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'extra_link_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-stacked .eg-icon-box .eg-link-btn,
					 {{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'extra_link_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-icon-box .eg-link-btn',
			]
		);

		$this->start_controls_tabs( 'tabs_extra_link' );

		$this->start_controls_tab(
			'tab_extra_link_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'extra_link_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box .eg-link-btn' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extra_link_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-stacked .eg-icon-box .eg-link-btn' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'extra_link_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_extra_link_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'extra_link_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box .eg-link-btn:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extra_link_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-stacked .eg-icon-box .eg-link-btn:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'extra_link_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--extra-link-view-framed .eg-icon-box .eg-link-btn:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_layout_section_controls();
		$this->register_design_icon_section_controls();
		$this->register_design_content_section_controls();
		$this->register_design_custom_link_section_controls();
	}

	public function render_element_header() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute( 'wrapper', 'class', 'eg-icon-box' );
		?>
			<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {

		?>
			</div>
		<?php
	}

	public function render_icon() {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
			// add old default
			$settings['icon'] = 'fa fa-star';
		}

		if ( ! empty( $settings['icon'] ) ) {
			$this->add_render_attribute( 'icon', 'class', $settings['icon'] );
			$this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
		}

		$migrated = isset( $settings['__fa4_migrated']['selected_icon'] );
		$is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

		if ( $is_new || $migrated ) {
			Icons_Manager::render_icon( $settings['selected_icon'], [ 'aria-hidden' => 'true' ] );
		} else {
			?>
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			<?php
		}
	}

	public function render_link_icon() {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
			// add old default
			$settings['icon'] = 'fa fa-star';
		}

		if ( ! empty( $settings['icon'] ) ) {
			$this->add_render_attribute( 'icon', 'class', $settings['icon'] );
			$this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
		}

		$migrated = isset( $settings['__fa4_migrated']['link_selected_icon'] );
		$is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

		if ( $is_new || $migrated ) {
			Icons_Manager::render_icon( $settings['link_selected_icon'], [ 'aria-hidden' => 'true' ] );
		} else {
			?>
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			<?php
		}
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		if( '' !== $settings['custom_link'] ) {
			$this->add_link_attributes( 'custom-link-url', $settings['link_url'] );
		}

		$this->render_element_header();

		?>

		<div class="eg-icon-wrap">
			<div class="eg-icon">
				<?php $this->render_icon(); ?>
			</div>
		</div>
		<div class="eg-content">
			<?php
				if( $settings['title'] ) {
					if( $settings['custom_link'] == 'title_url' ) {
						echo '<h3 class="eg-title">
										<a ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>' . $settings['title'] . '</a>
									</h3>';
					} else {
						echo '<h3 class="eg-title">' . $settings['title'] . '</h3>';
					}
				}

				if( $settings['desc'] ) {
					echo '<div class="eg-desc">' . $settings['desc'] . '</div>';
				}

				if( $settings['custom_link'] == 'extra_link' ) {
					if( !empty( $settings['link_selected_icon']['value'] ) ) {
						if( $settings['link_icon_align']  == 'right' ) {
							echo '<a class="eg-link-btn eg-link-btn--icon-after" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>' . $settings['link_text'];
								$this->render_link_icon();
							echo '</a>';
						} else {
							echo '<a class="eg-link-btn eg-link-btn--icon-before" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>';
							$this->render_link_icon();
							echo $settings['link_text'] . '</a>';
						}
					} else {
						echo '<a class="eg-link-btn" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>' . $settings['link_text'] . '</a>';
					}
				}
			?>
		</div>

		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
