<?php
namespace ElementorGemcrypto\Widgets\Icon_List;


use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Icon_List extends Widget_Base {

	public function get_name() {
		return 'eg-icon-list';
	}

	public function get_title() {
		return __( 'EG Icon List', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-icon-list' ];
	}

	public function get_script_depends() {
		return [];
	}

  protected function register_layout_section_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => __( 'Icon List', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $repeater = new Repeater();

    $repeater->add_control(
      'select_icon',
      [
        'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::ICONS,
        'default' => [
          'value' => 'fas fa-star',
          'library' => 'fa-solid',
        ],
        'fa4compatibility' => 'icon',
      ]
    );

    $repeater->add_control(
      'link',
      [
        'label' => esc_html__( 'Link', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::URL,
        'dynamic' => [
          'active' => false,
        ],
        'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'icon_list',
      [
        'label' => esc_html__( 'Items', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'selected_icon' => [
              'value' => 'fas fa-star',
              'library' => 'fa-solid',
            ],
          ],
          [
            'selected_icon' => [
              'value' => 'fas fa-times',
              'library' => 'fa-solid',
            ],
          ],
          [
            'selected_icon' => [
              'value' => 'fas fa-dot-circle-o',
              'library' => 'fa-solid',
            ],
          ],
        ],
        'title_field' => '',
      ]
    );

    $this->end_controls_section();

  }

  protected function register_design_layout_section_controls() {

    $this->start_controls_section(
      'icon_list_section',
      [
        'label' => esc_html__( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_control(
      'icon_list_space_between',
      [
        'label' => esc_html__( 'Space Between', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'max' => 50,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-icon-list-items .eg-icon-list .eg-icon-item:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_responsive_control(
      'icon_list_align',
      [
        'label' => esc_html__( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
				'default' => '',
        'options' => [
          'left' => [
            'title' => esc_html__( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => esc_html__( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => esc_html__( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'prefix_class' => 'eg-icon-list--layout-',
      ]
    );

    $this->end_controls_section();
  }

  protected function register_design_icon_section_controls() {
    $this->start_controls_section(
			'icon_list_style_section',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'icon_list_size',
			[
				'label' => esc_html__( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon-list .eg-icon-list-icon ' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-icon-list .eg-icon-list-icon svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_list_color',
			[
				'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-list-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon-list-icon svg' => 'fill: {{VALUE}};',
				],
				'default' => '',
			]
		);

		$this->add_control(
			'icon__list_color_hover',
			[
				'label' => esc_html__( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-list .eg-icon-list-icon:hover i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon-list .eg-icon-list-icon:hover svg' => 'fill: {{VALUE}};',
				],
			]
		);

    $this->end_controls_section();
  }

  protected function register_controls() {
    $this->register_layout_section_controls();
    $this->register_design_layout_section_controls();
    $this->register_design_icon_section_controls();
  }

  public function render_element_header() {

    $this->add_render_attribute( 'wrapper', 'class', 'eg-icon-list-items' );
    ?>
      <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
    <?php
  }

  public function render_element_footer() {
    ?>
      </div>
    <?php
  }

  protected function render() {
    $settings = $this->get_settings_for_display();
		$fallback_defaults = [
			'fa fa-star',
			'fa fa-times',
			'fa fa-dot-circle-o',
		];

    $this->render_element_header();
    ?>
    <ul class="eg-icon-list">
    <?php
    foreach ( $settings['icon_list'] as $index => $item ) :

			$migration_allowed = Icons_Manager::is_migration_allowed();

			if (  ! empty( $item['icon'] ) || ! empty($item['select_icon']['value']) ) {
      ?>
      <li class="eg-icon-item">
        <?php
        if ( ! empty( $item['link']['url'] ) ) {
          $link_key = 'link_' . $index;
          $this->add_link_attributes( $link_key, $item['link'] );
          ?>
          <a <?php $this->print_render_attribute_string( $link_key ); ?>>
          <?php
        }

				if ( ! isset( $item['icon'] ) && ! $migration_allowed ) {
					$item['icon'] = isset( $fallback_defaults[ $index ] ) ? $fallback_defaults[ $index ] : 'fa fa-star';
				}

				$migrated = isset( $item['__fa4_migrated']['select_icon'] );
				$is_new = empty( $item['icon'] ) && $migration_allowed;

				echo '<span class="eg-icon-list-icon">';
				if ( $is_new || $migrated ) {
					Icons_Manager::render_icon( $item['select_icon'], [ 'aria-hidden' => 'true' ] );
				} else {
					?>
						<i class="<?php echo esc_attr( $item['icon'] ); ?>"></i>
					<?php
				}
				echo '</span>';
        ?>
        <?php if ( ! empty( $item['link']['url'] ) ) : ?>
          </a>
        <?php endif; ?>
      </li>
      <?php
			}
    endforeach;
    ?>
    </ul>
    <?php

    $this->render_element_footer();

  }

  protected function content_template() {

  }

}




 ?>
