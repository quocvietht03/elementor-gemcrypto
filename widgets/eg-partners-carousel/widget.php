<?php
namespace ElementorGemcrypto\Widgets\Partners_Carousel;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Css_Filter;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Partners_Carousel extends Widget_Base {

	public function get_name() {
		return 'eg-partners-carousel';
	}

	public function get_title() {
		return __( 'EG Partners Carousel', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-widget-carousel' ];
	}

	public function get_script_depends() {
		return ['eg-swiper', 'eg-widget-carousel'];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

    $this->add_responsive_control(
			'sliders_per_view',
			[
				'label' => __( 'Slides Per View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '4',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],

			]
		);

		$this->add_control(
			'sliders_per_column',
			[
				'label' => __( 'Slides Per Column', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				],
			]
		);

    $repeater = new Repeater();

    $repeater->add_control(
			'list_image', [
				'label' => __( 'Thumbnail', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

    $repeater->add_control(
			'list_custom_url', [
				'label' => __( 'Custom Url', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
        'label_block' => true,
				'default' => '',
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'List', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
            'list_image' => Utils::get_placeholder_image_src(),
            'list_custom_url' => '',
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_custom_url' => '',
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_custom_url' => '',
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_custom_url' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_custom_url' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_custom_url' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_custom_url' => '',
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_custom_url' => '',
					],
				],
        'title_field' => '',
			]
		);

		$this->end_controls_section();
	}

  protected function register_additional_section_controls() {
		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'bearsthemes-addons' ),
			]
		);

    $this->add_control(
			'navigation',
			[
				'label' => __( 'Prev & Next Button', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'nav_btn_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'navigation!' => '',
				],
				'prefix_class' => 'eg-swiper-button--view-',
			]
		);

    $this->add_control(
			'pagination',
			[
				'label' => __( 'Dots', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Transition Duration', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'loop',
			[
				'label' => __( 'Infinite Loop', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_latyout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_responsive_control(
			'space_between',
			[
				'label' => __( 'Space Between', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'size' => 30,
				],
				'selectors' => [
					'{{WRAPPER}} .eg-thumbnail:not(:first-child)' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'items_space_between',
			[
				'label' => __( 'Items Space Between', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'size' => 30,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-item:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'sliders_per_column!' => '1',
				],
			]
		);


		$this->end_controls_section();
	}


  protected function register_design_image_section_controls() {
		$this->start_controls_section(
			'section_design_image',
			[
				'label' => __( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'image_background_color',
			[
				'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-thumbnail' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'image_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .eg-thumbnail' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-thumbnail' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'image_box_shadow',
				'selector' => '{{WRAPPER}} .eg-thumbnail',
			]
		);

		$this->start_controls_tabs( 'thumbnail_effects_tabs' );

		$this->start_controls_tab( 'normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_filters',
				'selector' => '{{WRAPPER}} .eg-thumbnail img',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_hover_filters',
				'selector' => '{{WRAPPER}} .eg-thumbnail:hover img',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	protected function register_design_navigation_section_controls() {
		$this->start_controls_section(
			'section_design_navigation',
			[
				'label' => __( 'Prev & Next Button', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation!' => '',
				]
			]
		);

		$this->add_responsive_control(
			'arrows_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-swiper-button svg' => 'width: {{SIZE}}{{UNIT}}; height: auto;',
				],
			]
		);

		$this->add_control(
			'icon_border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view' => 'framed',
				],
			]
		);

		$this->add_control(
			'icon_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view!' => '',
				],
			]
		);

		$this->add_responsive_control(
			'icon_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'padding-top: {{TOP}}{{UNIT}}; padding-right: {{RIGHT}}{{UNIT}}; padding-bottom: {{BOTTOM}}{{UNIT}}; padding-left: {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view!' => '',
				],
			]
		);

		$this->start_controls_tabs( 'navigation_style' );

		$this->start_controls_tab(
			'navigation_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'navigation_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'navigation_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_pagination_section_controls() {
		$this->start_controls_section(
			'section_design_pagination',
			[
				'label' => __( 'Dots', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'pagination!' => '',
				]
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'pagination_space_between',
			[
				'label' => esc_html__( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'margin-left: {{SIZE}}{{UNIT}}; margin-right: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
			]
		);

		$this->start_controls_tabs( 'pagination_style' );

		$this->start_controls_tab(
			'pagination_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet:not(.swiper-pagination-bullet-active)' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'pagination_active',
			[
				'label' => __( 'Active', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color_active',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();
    $this->register_additional_section_controls();

		$this->register_design_latyout_section_controls();
    $this->register_design_image_section_controls();
		$this->register_design_navigation_section_controls();
		$this->register_design_pagination_section_controls();
	}

  protected function swiper_data() {
		$settings = $this->get_settings_for_display();
		$widget_id = $this->get_id();

		$slides_per_view = $settings['sliders_per_view'];
		$slides_per_view_tablet = isset( $settings['sliders_per_view_tablet'] ) ? $settings['sliders_per_view_tablet'] : 2;
		$slides_per_view_mobile = isset( $settings['sliders_per_view_mobile'] ) ? $settings['sliders_per_view_mobile'] : 1;

		$space_between = ( '' !== $settings['space_between']['size'] ) ? $settings['space_between']['size'] : 30;
		$space_between_tablet = ( isset( $settings['space_between_tablet']['size'] ) && '' !== $settings['space_between_tablet']['size'] ) ? $settings['space_between_tablet']['size'] : $space_between;
		$space_between_mobile = ( isset( $settings['space_between_mobile']['size'] ) && '' !== $settings['space_between_mobile']['size'] ) ? $settings['space_between_mobile']['size'] : $space_between_tablet;

		$swiper_data = array(
			'slidesPerView' => $slides_per_view_mobile,
			'spaceBetween' => $space_between_mobile,
			'speed' => $settings['speed'],
			'loop' => $settings['loop'] == 'yes' ? true : false,
			'breakpoints' => array(
				767 => array(
				  'slidesPerView' => $slides_per_view_tablet,
				  'spaceBetween' => $space_between_tablet,
				),
				1025 => array(
				  'slidesPerView' => $slides_per_view,
				  'spaceBetween' => $space_between,
				)
			),

		);

		

		if( $settings['navigation'] === 'yes' ) {
			$swiper_data['navigation'] = array(
				'nextEl' => '.eg-swiper-button-next-'.$widget_id,
				'prevEl' => '.eg-swiper-button-prev-'.$widget_id,
			);
		}

		if( $settings['pagination'] === 'yes' ) {
			$swiper_data['pagination'] = array(
				'el' => '.eg-swiper-pagination-'.$widget_id,
				'type' => 'bullets',
				'clickable' => true,
			);
		}

		if( $settings['autoplay'] === 'yes' ) {
			$swiper_data['autoplay'] = array(
				'delay' => $settings['autoplay_speed'],
			);
		}

		return $swiper_json = json_encode($swiper_data);
	}

	public function render_element_header() {
    $settings = $this->get_settings_for_display();


    $classes = 'swiper-container eg-partners';

    if( $settings['navigation'] === 'yes' ) {
      $classes .= ' has-navigation';
    }

    if( $settings['pagination'] === 'yes' ) {
      $classes .= ' has-pagination';
    }

		$this->add_render_attribute( 'wrapper', 'class', $classes );

    $this->add_render_attribute( 'wrapper', 'data-swiper', $this->swiper_data() );

    echo '<div ' . $this->get_render_attribute_string( 'wrapper' ) . '>';
	}

	public function render_element_footer() {
    $settings = $this->get_settings_for_display();
		$widget_id = $this->get_id();

		echo '</div>';

    if( $settings['navigation'] === 'yes' ) {

			$select_icon_pre = 'select_icon_prev';
			$select_icon_next = 'select_icon_next';

			?>
			<div class="eg-swiper-button eg-swiper-button-prev eg-swiper-button-prev-<?php echo $widget_id; ?>">
				<i class="fa fa-chevron-left"></i>
			</div>
			<div class="eg-swiper-button eg-swiper-button-next eg-swiper-button-next-<?php echo $widget_id; ?>">
				<i class="fa fa-chevron-right"></i>
			</div>
			<?php
    }

    if( $settings['pagination'] === 'yes' ) {
      echo '<div class="eg-swiper-pagination eg-swiper-pagination-'.$widget_id.'"></div>';
    }
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		?>

		<div class="swiper-wrapper eg-list-partners">
			<?php
        if( !empty( $settings['list'] ) ) {
					$total = count($settings['list']);
					$count = 0;
          foreach ( $settings['list'] as $key => $value ) {
            $attachment = wp_get_attachment_image_src( $value['list_image']['id'], 'full' );
            $thumbnail = !empty( $attachment ) ? $attachment[0] : $value['list_image']['url'];

						$count++;

						if( $count == 1 ) {
							echo '<div class="swiper-slide elementor-item">';
						}
                echo '<div class="eg-thumbnail">';
                  if( !empty( $value['list_custom_url'] ) ) {
                    echo '<a href="' . esc_url( $value['list_custom_url'] ) . '"><img src=" ' . esc_url( $thumbnail ) . ' " alt=""></a>';
                  } else {
                    echo '<img src=" ' . esc_url( $thumbnail ) . ' " alt="">';
                  }
                echo '</div>';

						if( $count == $settings['sliders_per_column'] || $total == $key ) {
							echo '</div>';
						}

						if( $count == $settings['sliders_per_column'] ) $count = 0;

          }
        }
      ?>
		</div>

		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}

}
