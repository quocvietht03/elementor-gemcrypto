<?php
namespace ElementorGemcrypto\Widgets\Account_Link;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Account_Link extends Widget_Base {

	public function get_name() {
		return 'eg-account-link';
	}

	public function get_title() {
		return __( 'EG Account Link', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-account-link' ];
	}

	public function get_script_depends() {
		return [];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
      'heading_register',
      [
        'label' => __( 'Register', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'text_register',
      [
        'label' => __( 'Text Register', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXT,
        'label_block' => true,
        'default' => __( 'Register', 'elementor-gemcrypto' ),
      ]
    );

		$this->add_control(
			'link_register',
			[
				'label' => esc_html__( 'Link Register', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
				'default' => [
					'url' => esc_url(wp_registration_url()),
				],
			]
		);

		$this->add_control(
			'icon_register',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'skin' => 'inline',
				'label_block' => false,
			]
		);

		$this->add_control(
			'icon_align_register',
			[
				'label' => esc_html__( 'Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
					'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_register[value]!' => '',
				],
			]
		);

    $this->add_control(
      'heading_login',
      [
        'label' => __( 'Login', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'text_login',
      [
        'label' => __( 'Text Login', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXT,
        'label_block' => true,
        'default' => __( 'Login', 'elementor-gemcrypto' ),
      ]
    );

		$this->add_control(
			'link_login',
			[
				'label' => esc_html__( 'Link Login', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
				'default' => [
					'url' => esc_url(wp_login_url()),
				],
			]
		);

		$this->add_control(
			'icon_login',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'skin' => 'inline',
				'label_block' => false,
			]
		);

		$this->add_control(
			'icon_align_login',
			[
				'label' => esc_html__( 'Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
					'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_login[value]!' => '',
				],
			]
		);

		$this->add_control(
			'icon_space_login',
			[
				'label' => esc_html__( 'Icon Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'icon_login[value]!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_logout',
			[
				'label' => __( 'Logout', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'text_logout',
			[
				'label' => __( 'Text Logout', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Logout', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'link_logout',
			[
				'label' => esc_html__( 'Link Logout', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
				'default' => [
					'url' => esc_url(wp_logout_url()),
				],
			]
		);

		$this->add_control(
			'icon_logout',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'skin' => 'inline',
				'label_block' => false,
			]
		);

    $this->add_control(
      'icon_align_logout',
      [
        'label' => esc_html__( 'Icon Position', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => 'left',
        'options' => [
          'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
          'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
        ],
        'condition' => [
          'icon_logout[value]!' => '',
        ],
      ]
    );

		$this->add_control(
			'icon_space_logout',
			[
				'label' => esc_html__( 'Icon Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'icon_logout[value]!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_layout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => esc_html__( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => '',
				'options' => [
					'left'    => [
						'title' => esc_html__( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-account-wrapper' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_icon_section_controls() {
		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);


		$this->add_control(
			'box_border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-account-link' => 'border-width: {{SIZE}}{{UNIT}} ;',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-account-link' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .eg-account-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->start_controls_tabs( 'tabs_box' );

		$this->start_controls_tab(
			'tab_box_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-account-link,
					{{WRAPPER}} .eg-account-link' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'box_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-account-link' => 'background-color: {{VALUE}};',
				],
			]
		);


		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_box_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-account-link:hover,
					{{WRAPPER}} .eg-account-link:focus' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'box_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-account-link:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Text', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_text',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-account-link',
			]
		);

		$this->start_controls_tabs( 'tabs_text' );

		$this->start_controls_tab(
			'tab_text_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-account-link' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_text_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'text_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-account-link:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();


		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_layout_section_controls();
		$this->register_design_icon_section_controls();
		$this->register_design_content_section_controls();
	}

	public function render_element_header() {

		$this->add_render_attribute( 'wrapper', 'class', 'eg-account-wrapper' );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {
		?>
			</div>
		<?php
	}

	public function render_icon($icon) {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
			// add old default
			$settings['icon'] = 'fa fa-star';
		}

		if ( ! empty( $settings['icon'] ) ) {
			$this->add_render_attribute( 'icon', 'class', $settings['icon'] );
			$this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
		}

		$migrated = isset( $settings['__fa4_migrated'][$icon] );
		$is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

		if ( $is_new || $migrated ) {
			Icons_Manager::render_icon( $settings[$icon], [ 'aria-hidden' => 'true' ] );
		} else {
			?>
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			<?php
		}
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( ! empty( $settings['link_login']['url'] ) ) {
			$this->add_link_attributes( 'login', $settings['link_login'] );
			$this->add_render_attribute( 'login', 'class', 'eg-account-link eg-account-login' );
		}

		if ( ! empty( $settings['link_logout']['url'] ) ) {
			$this->add_link_attributes( 'logout', $settings['link_logout'] );
			$this->add_render_attribute( 'logout', 'class', 'eg-account-link eg-account-logout' );
		}

		if ( ! empty( $settings['link_register']['url'] ) ) {
			$this->add_link_attributes( 'register', $settings['link_register'] );
			$this->add_render_attribute( 'register', 'class', 'eg-account-link eg-account-register' );
		}

		$has_icon = (!is_user_logged_in())? $settings['icon_login']['value'] : $settings['icon_logout']['value'];
		$text = (!is_user_logged_in())? $settings['text_login'] : $settings['text_logout'];
		$icon_align = (!is_user_logged_in())? $settings['icon_align_login'] : $settings['icon_align_logout'];
		$icon = (is_user_logged_in())? 'icon_logout' : 'icon_login' ;

		$this->render_element_header();
		?>
		<?php if (!is_user_logged_in()): ?>
			<?php if (!empty($settings['icon_register']['value'])): ?>
				<a <?php  $this->print_render_attribute_string( 'register' ); ?> >
					<span class="eg-account-content-wrapper">
						<?php if ($settings['icon_align_register'] == 'left'): ?>
							<span class="eg-account-icon eg-align-icon-left">
								<?php $this->render_icon('icon_register'); ?>
							</span>
							<span class="eg-account-text">
								<?php echo $settings['text_register']; ?>
							</span>
						<?php endif; ?>
						<?php if ($settings['icon_align_register'] == 'right'): ?>
							<span class="eg-account-text">
								<?php echo $settings['text_register']; ?>
							</span>
							<span class="eg-account-icon eg-align-icon-right">
								<?php $this->render_icon('icon_register'); ?>
							</span>
						<?php endif; ?>
					</span>
				</a>
			<?php else: ?>
				<a <?php $this->print_render_attribute_string( 'register' ); ?>>
					<span class="eg-account-content-wrapper">
						<span class="eg-account-text">
							<?php echo $settings['text_register']; ?>
						</span>
					</span>
				</a>
			<?php endif; ?>
		<?php endif; ?>
		<?php if (!empty($has_icon)): ?>
			<a <?php (!is_user_logged_in())? $this->print_render_attribute_string( 'login' ) : $this->print_render_attribute_string( 'logout' ); ?> >
				<span class="eg-account-content-wrapper">
					<?php if ($icon_align == 'left'): ?>
						<span class="eg-account-icon eg-align-icon-left">
							<?php $this->render_icon($icon); ?>
						</span>
						<span class="eg-account-text">
							<?php echo $text; ?>
						</span>
					<?php endif; ?>
					<?php if ($icon_align == 'right'): ?>
						<span class="eg-account-text">
							<?php echo $text; ?>
						</span>
						<span class="eg-account-icon eg-align-icon-right">
							<?php $this->render_icon($icon); ?>
						</span>
					<?php endif; ?>
				</span>
			</a>
		<?php else: ?>
			<a <?php (!is_user_logged_in())? $this->print_render_attribute_string( 'login' ) : $this->print_render_attribute_string( 'logout' ); ?>>
				<span class="eg-account-content-wrapper">
					<span class="eg-account-text">
						<?php echo $text; ?>
					</span>
				</span>
			</a>
		<?php endif; ?>


		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
