<?php
namespace ElementorGemcrypto\Widgets\Posts\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Sidu extends Skin_Base {

  protected function _register_controls_actions() {
		add_action( 'elementor/element/eg-posts/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/before_section_end', [ $this, 'register_design_latyout_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/after_section_end', [ $this, 'register_design_box_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/after_section_end', [ $this, 'register_design_content_section_controls' ] );

	}

	public function get_id() {
		return 'skin-sidu';
	}

	public function get_title() {
		return __( 'Sidu', 'elementor-gemcrypto' );
	}

  public function register_layout_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'prefix_class' => 'elementor-grid%s-',
			]
		);

    $this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
			]
		);

    $this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail',
				'default' => 'medium',
				'exclude' => [ 'custom' ],
			]
		);

    $this->add_responsive_control(
      'image_height',
      [
        'label' => __( 'Height', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .elementor-grid .elementor-item' => 'min-height: {{VALUE}}px;',
        ],
      ]
    );

    $this->add_control(
			'show_title',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_date',
			[
				'label' => __( 'Date', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

    $this->add_control(
			'show_category',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => __( 'Meta', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

	}

  public function register_design_latyout_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'text-align: {{VALUE}};',
				],
			]
		);

	}

  public function register_design_box_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-radius: {{SIZE}}{{UNIT}}',
				],
			]
		);

    $this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post__content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'bg_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-post',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-post:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  public function register_design_content_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
        'condition' => [
					'skin_sidu_show_title!' => '',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__title' => 'color: {{VALUE}};',
				],
        'condition' => [
					'skin_sidu_show_title!' => '',
				],
			]
		);

		$this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					' {{WRAPPER}} .eg-post__title a:hover' => 'color: {{VALUE}};',
				],
        'condition' => [
					'skin_sidu_show_title!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__title',
        'condition' => [
					'skin_sidu_show_title!' => '',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'feature_title_typography',
        'label' => __( 'Feature Typography', 'elementor-gemcrypto' ),
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post:first-child .eg-post__title',
        'condition' => [
					'skin_sidu_show_title!' => '',
				],
			]
		);

		$this->add_control(
			'heading_date_style',
			[
				'label' => __( 'Date', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
        'condition' => [
					'skin_sidu_show_date!' => '',
				],
			]
		);

		$this->add_control(
			'date_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__date' => 'color: {{VALUE}};',
				],
        'condition' => [
					'skin_sidu_show_date!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'date_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__date',
        'condition' => [
					'skin_sidu_show_date!' => '',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'feature_date_typography',
        'label' => __( 'Feature Typography', 'elementor-gemcrypto' ),
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post:first-child .eg-post__date',
        'condition' => [
					'skin_sidu_show_date!' => '',
				],
			]
		);

    $this->add_control(
      'heading_category_style',
      [
        'label' => __( 'Category', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
        'condition' => [
          'skin_sidu_show_category!' => '',
        ],
      ]
    );

    $this->add_control(
      'category_color',
      [
        'label' => __( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-post__category' => 'color: {{VALUE}};',
        ],
        'condition' => [
          'skin_sidu_show_category!' => '',
        ],
      ]
    );

    $this->add_control(
      'category_color_hover',
      [
        'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-post__category a:hover' => 'color: {{VALUE}};',
        ],
        'condition' => [
          'skin_sidu_show_category!' => '',
        ],
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'category_typography',
        'default' => '',
        'selector' => '{{WRAPPER}} .eg-post__category',
        'condition' => [
          'skin_sidu_show_category!' => '',
        ],
      ]
    );

    $this->add_control(
			'heading_meta_style',
			[
				'label' => __( 'Meta', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'skin_sidu_show_meta!' => '',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__meta' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_sidu_show_meta!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__meta',
				'condition' => [
					'skin_sidu_show_meta!' => '',
				],
			]
		);

		$this->add_control(
			'meta_space_between',
			[
				'label' => __( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post__meta img' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'skin_sidu_show_meta!' => '',
				],
			]
		);

		$this->end_controls_section();
  }

  protected function render_post() {
    $featured_img_url = get_the_post_thumbnail_url( get_the_ID(), $this->parent->get_instance_value_skin('thumbnail_size') );

		?>
			<article id="post-<?php the_ID();  ?>" <?php post_class( 'elementor-item eg-post' ); ?> style="<?php echo 'background-image: url(' . $featured_img_url . ');' ?>">
        <div class="eg-post__overlay"></div>

				<div class="eg-post__content">
          <div class="eg-post__box">
					<?php if( '' !== $this->parent->get_instance_value_skin('show_date') ) { ?>
						<div class="eg-post__date">
              <i class="fa fa-calendar" aria-hidden="true"></i>
				      <?php echo get_the_date( get_option( 'date_format' ) ); ?>
				    </div>
					<?php } ?>

          <?php
						if( '' !== $this->parent->get_instance_value_skin('show_title') ) {
							the_title( '<h3 class="eg-post__title"><a href="' . get_the_permalink() . '">', '</a></h3>' );
						}
					?>

          <?php if( '' !== $this->parent->get_instance_value_skin('show_meta') ) { ?>
            <div class="eg-post__meta">
              <?php echo get_avatar(get_the_author(), 40); ?>
              <?php echo __( '<span>By '.get_the_author().'</span>', 'elementor-gemcrypto' )  ; ?>
            </div>
          <?php } ?>
          <?php
            if ( has_category() && '' !== $this->parent->get_instance_value_skin('show_category') ) {
              echo '<div class="eg-post__category">';
                the_category( ' ' );
              echo '</div>';
            }
           ?>
           </div>
				</div>
			</article>
		<?php
	}

	public function render() {
    $query = $this->parent->query_posts();

		$this->parent->render_element_header();

			if ( $query->have_posts() ) {

				$this->parent->render_loop_header();

					while ( $query->have_posts() ) {
						$query->the_post();

						$this->render_post();
					}

				$this->parent->render_loop_footer();

			} else {
			    // no posts found
			}
			wp_reset_postdata();

		$this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
