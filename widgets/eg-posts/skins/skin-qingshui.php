<?php
namespace ElementorGemcrypto\Widgets\Posts\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Qingshui extends Skin_Base {

  protected function _register_controls_actions() {
		add_action( 'elementor/element/eg-posts/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/before_section_end', [ $this, 'register_design_latyout_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/after_section_end', [ $this, 'register_design_box_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/after_section_end', [ $this, 'register_design_image_section_controls' ] );
    add_action( 'elementor/element/eg-posts/section_design_layout/after_section_end', [ $this, 'register_design_content_section_controls' ] );

	}

	public function get_id() {
		return 'skin-qingshui';
	}

	public function get_title() {
		return __( 'Qingshui', 'elementor-gemcrypto' );
	}

  public function register_layout_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'prefix_class' => 'elementor-grid%s-',
			]
		);

    $this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
			]
		);

		$this->add_control(
			'show_thumbnail',
			[
				'label' => __( 'Thumbnail', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail',
				'default' => 'medium',
				'exclude' => [ 'custom' ],
				'condition' => [
					'skin_qingshui_show_thumbnail!'=> '',
				],
			]
		);

    $this->add_responsive_control(
			'image_ratio',
			[
				'label' => __( 'Image Ratio', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0.6,
				],
				'range' => [
					'px' => [
						'min' => 0.3,
						'max' => 2,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post__featured' => 'padding-bottom: calc( {{SIZE}} * 100% );',
				],
				'condition' => [
					'skin_qingshui_show_thumbnail!'=> '',
				],
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

    $this->add_control(
			'show_date',
			[
				'label' => __( 'Date', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

    $this->add_control(
			'show_category',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => __( 'Meta', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'elementor-gemcrypto' ),
				'label_off' => __( 'Hide', 'elementor-gemcrypto' ),
				'default' => 'yes',
			]
		);

	}

  public function register_design_latyout_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'text-align: {{VALUE}};',
				],
			]
		);

	}

  public function register_design_box_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-radius: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post__content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'bg_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-post',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-post:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-post:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  public function register_design_image_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->start_controls_section(
			'section_design_image',
			[
				'label' => __( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-post__featured' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'thumbnail_effects_tabs' );

		$this->start_controls_tab( 'normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_filters',
				'selector' => '{{WRAPPER}} .eg-post__featured img',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_hover_filters',
				'selector' => '{{WRAPPER}} .eg-post:hover .eg-post__featured img',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

    $this->end_controls_section();
  }

  public function register_design_content_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'skin_qingshui_show_title!' => '',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__title' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_title!' => '',
				],
			]
		);

		$this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					' {{WRAPPER}} .eg-post__title a:hover' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_title!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__title',
				'condition' => [
					'skin_qingshui_show_title!' => '',
				],
			]
		);

    $this->add_control(
			'heading_date_style',
			[
				'label' => __( 'Date', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_control(
			'date_day_color',
			[
				'label' => __( 'Day Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__date .day' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_control(
			'date_day_bg_color',
			[
				'label' => __( 'Day Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__date' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'date_day_typography',
        'label' => __( 'Day Typography', 'elementor-gemcrypto' ),
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__date .day',
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_control(
			'date_month_color',
			[
				'label' => __( 'Month Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__date month' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_control(
			'date_month_bg_color',
			[
				'label' => __( 'Month Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__date .month' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'date_month_typography',
        'label' => __( 'Month Typography', 'elementor-gemcrypto' ),
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__date .month',
				'condition' => [
					'skin_qingshui_show_date!' => '',
				],
			]
		);

    $this->add_control(
			'heading_category_style',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'skin_qingshui_show_category!' => '',
				],
			]
		);

		$this->add_control(
			'category_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__category' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_category!' => '',
				],
			]
		);

    $this->add_control(
			'category_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__category a:hover' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_category!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'category_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__category',
				'condition' => [
					'skin_qingshui_show_category!' => '',
				],
			]
		);

		$this->add_control(
			'heading_meta_style',
			[
				'label' => __( 'Meta', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'skin_qingshui_show_meta!' => '',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-post__meta' => 'color: {{VALUE}};',
				],
				'condition' => [
					'skin_qingshui_show_meta!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-post__meta',
				'condition' => [
					'skin_qingshui_show_meta!' => '',
				],
			]
		);

		$this->add_control(
			'meta_space_between',
			[
				'label' => __( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-post__meta li:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'skin_qingshui_show_meta!' => '',
				],
			]
		);

		$this->end_controls_section();
  }

  protected function render_post() {
    $classes = 'elementor-item eg-post';

    if( '' !== $this->parent->get_instance_value_skin('show_date') ) {
      $classes .= ' has-date';
    }

		?>
    <article id="post-<?php the_ID();  ?>" <?php post_class( $classes ); ?>>
      <?php if( '' !== $this->parent->get_instance_value_skin('show_thumbnail' ) ) { ?>
        <div class="eg-post__thumbnail">
          <a href="<?php the_permalink() ?>">
            <div class="eg-post__featured"><?php the_post_thumbnail( $this->parent->get_instance_value_skin('thumbnail_size') ); ?></div>
          </a>

          <?php if( '' !== $this->parent->get_instance_value_skin('show_date') ) { ?>
            <div class="eg-post__date">
              <?php echo '<span class="day">' . get_the_date( 'd' ) . '</span><span class="month">' . get_the_date( 'M' ) . '</span>'; ?>
            </div>
          <?php } ?>
        </div>
      <?php } ?>

      <div class="eg-post__content">
        <?php
          if ( has_category() && '' !== $this->parent->get_instance_value_skin('show_category') ) {
            echo '<div class="eg-post__category">';
              echo '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tags" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-tags fa-w-20 fa-3x"><path fill="currentColor" d="M497.941 225.941L286.059 14.059A48 48 0 0 0 252.118 0H48C21.49 0 0 21.49 0 48v204.118a48 48 0 0 0 14.059 33.941l211.882 211.882c18.744 18.745 49.136 18.746 67.882 0l204.118-204.118c18.745-18.745 18.745-49.137 0-67.882zM112 160c-26.51 0-48-21.49-48-48s21.49-48 48-48 48 21.49 48 48-21.49 48-48 48zm513.941 133.823L421.823 497.941c-18.745 18.745-49.137 18.745-67.882 0l-.36-.36L527.64 323.522c16.999-16.999 26.36-39.6 26.36-63.64s-9.362-46.641-26.36-63.64L331.397 0h48.721a48 48 0 0 1 33.941 14.059l211.882 211.882c18.745 18.745 18.745 49.137 0 67.882z" class=""></path></svg>';
              the_category( ', ' );
            echo '</div>';
          }

          if( '' !== $this->parent->get_instance_value_skin('show_title') ) {
            the_title( '<h3 class="eg-post__title"><a href="' . get_the_permalink() . '">', '</a></h3>' );
          }
        ?>

        <?php if( '' !== $this->parent->get_instance_value_skin('show_meta') ) { ?>
          <ul class="eg-post__meta">
            <li>
              <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-user fa-w-14 fa-3x"><path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z" class=""></path></svg>
              <?php echo __( 'By ', 'elementor-gemcrypto' ) . '<span>' . get_the_author() . '</span>'; ?>
            </li>
            <li>
              <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="comment" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment fa-w-16 fa-3x"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29 7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160-93.3 160-208 160z" class=""></path></svg>
	            <?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?>
	          </li>
          </ul>
        <?php } ?>
      </div>
    </article>
		<?php
	}

	public function render() {
    $query = $this->parent->query_posts();

		$this->parent->render_element_header();

			if ( $query->have_posts() ) {

				$this->parent->render_loop_header();

					while ( $query->have_posts() ) {
						$query->the_post();

						$this->render_post();
					}

				$this->parent->render_loop_footer();

			} else {
			    // no posts found
			}
			wp_reset_postdata();

		$this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
