<?php
namespace ElementorGemcrypto\Widgets\Testimonials_Carousel\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Yachi extends Skin_Base {

  protected function _register_controls_actions() {
    add_action( 'elementor/element/eg-testimonials-carousel/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );

	}

	public function get_id() {
		return 'skin-yachi';
	}

	public function get_title() {
		return __( 'Yachi', 'elementor-gemcrypto' );
	}

  public function register_layout_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->add_responsive_control(
			'sliders_per_view',
			[
				'label' => __( 'Slides Per View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
			]
		);

		$this->add_control(
			'sliders_per_column',
			[
				'label' => __( 'Slides Per Column', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				],
			]
		);


  }



  public function render_element_item( $value ) {

    echo '<div class="elementor-item eg-testimonial">';

			echo '<div class="eg-content">';
        if( !empty( $value['list_desc'] ) ) {
          echo '<div class="eg-desc">' . $value['list_desc'] . '</div>';
        }
        echo '<div class="eg-info-inner">';
          echo '<span class="eg-qoute-icon">
                  <svg width="38" height="32" viewBox="0 0 38 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.1429 19.1429C17.1429 16.7768 15.2232 14.8571 12.8571 14.8571H7.85714C6.67411 14.8571 5.71429 13.8973 5.71429 12.7143V12C5.71429 8.85268 8.28125 6.28571 11.4286 6.28571H12.8571C13.6384 6.28571 14.2857 5.63839 14.2857 4.85714V2C14.2857 1.21875 13.6384 0.571427 12.8571 0.571427H11.4286C5.13393 0.571427 0 5.70536 0 12V27.7143C0 30.0804 1.91964 32 4.28571 32H12.8571C15.2232 32 17.1429 30.0804 17.1429 27.7143V19.1429ZM37.1429 19.1429C37.1429 16.7768 35.2232 14.8571 32.8571 14.8571H27.8571C26.6741 14.8571 25.7143 13.8973 25.7143 12.7143V12C25.7143 8.85268 28.2813 6.28571 31.4286 6.28571H32.8571C33.6384 6.28571 34.2857 5.63839 34.2857 4.85714V2C34.2857 1.21875 33.6384 0.571427 32.8571 0.571427H31.4286C25.1339 0.571427 20 5.70536 20 12V27.7143C20 30.0804 21.9196 32 24.2857 32H32.8571C35.2232 32 37.1429 30.0804 37.1429 27.7143V19.1429Z" fill="#F64D52"/>
                  </svg>
                </span>';
          echo '<div class="eg-info">';
    				if( !empty( $value['list_name'] ) ) {
              echo '<h3 class="eg-name">' . $value['list_name'] . '</h3>';
    				}

            if( !empty( $value['list_job'] ) ) {
              echo '<div class="eg-job">' . $value['list_job'] . '</div>';
    				}
          echo '</div>';

        echo '</div>';

			echo '</div>';

		echo '</div>';
  }

  public function render() {

    	$this->parent->render_element_header();

      ?>
      <div class="swiper-wrapper eg-list-testimonials">
        <?php
          if( !empty($this->parent->get_settings('list') ) ) {
            $total = count( $this->parent->get_settings('list') );
            $count = 0;

            foreach ( $this->parent->get_settings('list') as $key => $value ) {
              $count++;

              if( $count == 1 ) {
                echo '<div class="swiper-slide">';
              }

              $this->render_element_item( $value );

              if( $count == $this->parent->get_instance_value_skin('sliders_per_column') || $total == $key ) {
                echo '</div>';
              }

              if( $count == $this->parent->get_instance_value_skin('sliders_per_column') ) $count = 0;
            }
          }
        ?>
      </div>
      <?php

      $this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
