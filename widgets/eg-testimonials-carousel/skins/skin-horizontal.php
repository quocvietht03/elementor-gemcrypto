<?php
namespace ElementorGemcrypto\Widgets\Testimonials_Carousel\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Horizontal extends Skin_Base {

  protected function _register_controls_actions() {
    add_action( 'elementor/element/eg-testimonials-carousel/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );
	}

	public function get_id() {
		return 'skin-horizontal';
	}

	public function get_title() {
		return __( 'Horizontal', 'elementor-gemcrypto' );
	}

  public function register_layout_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->add_responsive_control(
			'sliders_per_view',
			[
				'label' => __( 'Slides Per View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
			]
		);

		$this->add_control(
			'sliders_per_column',
			[
				'label' => __( 'Slides Per Column', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				],
			]
		);


  }



  public function render_element_item( $value ) {
    $attachment = wp_get_attachment_image_src( $value['list_image']['id'], 'full' );
    $thumbnail = !empty( $attachment ) ? $attachment[0] : $value['list_image']['url'];

    echo '<div class="elementor-item">';
      echo '<div class="eg-testimonial">';

  			echo '<div class="eg-thumbnail-wrap">
                <div class="eg-thumbnail">
                  <img src=" ' . esc_url( $thumbnail ) . ' " alt="">
                </div>
                <span class="eg-qoute-icon">
                  <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.57143 9.57143C8.57143 8.38839 7.61161 7.42857 6.42857 7.42857H3.92857C3.33705 7.42857 2.85714 6.94866 2.85714 6.35714V6C2.85714 4.42634 4.14063 3.14286 5.71429 3.14286H6.42857C6.8192 3.14286 7.14286 2.8192 7.14286 2.42857V0.999999C7.14286 0.609374 6.8192 0.285713 6.42857 0.285713H5.71429C2.56696 0.285713 0 2.85268 0 6V13.8571C0 15.0402 0.959821 16 2.14286 16H6.42857C7.61161 16 8.57143 15.0402 8.57143 13.8571V9.57143ZM18.5714 9.57143C18.5714 8.38839 17.6116 7.42857 16.4286 7.42857H13.9286C13.3371 7.42857 12.8571 6.94866 12.8571 6.35714V6C12.8571 4.42634 14.1406 3.14286 15.7143 3.14286H16.4286C16.8192 3.14286 17.1429 2.8192 17.1429 2.42857V0.999999C17.1429 0.609374 16.8192 0.285713 16.4286 0.285713H15.7143C12.567 0.285713 10 2.85268 10 6V13.8571C10 15.0402 10.9598 16 12.1429 16H16.4286C17.6116 16 18.5714 15.0402 18.5714 13.8571V9.57143Z" fill="#F7E406"/>
                  </svg>
                </span>
              </div>';

  			echo '<div class="eg-content">';
          echo '<div class="eg-desc-inner">';
          if( !empty( $value['list_desc'] ) ) {
            echo '<div class="eg-desc">' . $value['list_desc'] . '</div>';
          }

          if( !empty( $value['list_rating_number'] ) ) {
            echo $this->parent->render_stars($value);
  				}
          echo '</div>';
          echo '<div class="eg-info-inner">';
  				if( !empty( $value['list_name'] ) ) {
            echo '<h3 class="eg-name">' . $value['list_name'] . '</h3>';
  				}

          if( !empty( $value['list_job'] ) ) {
            echo '<div class="eg-job">' . $value['list_job'] . '</div>';
  				}
          echo '</div>';
  			echo '</div>';
      echo '</div>';
		echo '</div>';
  }

  public function render() {

    	$this->parent->render_element_header();

      ?>
      <div class="swiper-wrapper eg-list-testimonials">
        <?php
          if( !empty($this->parent->get_settings('list') ) ) {
            $total = count( $this->parent->get_settings('list') );
            $count = 0;

            foreach ( $this->parent->get_settings('list') as $key => $value ) {
              $count++;

              if( $count == 1 ) {
                echo '<div class="swiper-slide">';
              }

              $this->render_element_item( $value );

              if( $count == $this->parent->get_instance_value_skin('sliders_per_column') || $total == $key ) {
                echo '</div>';
              }

              if( $count == $this->parent->get_instance_value_skin('sliders_per_column') ) $count = 0;
            }
          }
        ?>
      </div>
      <?php

      $this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
