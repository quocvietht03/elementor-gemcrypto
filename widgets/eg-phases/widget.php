<?php
namespace ElementorGemcrypto\Widgets\Phases;


use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;



if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Phases extends Widget_Base {

	public function get_name() {
		return 'eg-phases';
	}

	public function get_title() {
		return __( 'EG Phases', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-phases', 'eg-show-more' ];
	}

	public function get_script_depends() {
		return ['eg-show-more'];
	}

  protected function register_layout_section_controls() {
    $this->start_controls_section(
      'phases_layout_section',
      [
        'label' => __( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_responsive_control(
      'columns',
      [
        'label' => __( 'Columns', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => '4',
        'tablet_default' => '3',
        'mobile_default' => '2',
        'options' => [
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
          '5' => '5',
          '6' => '6',
        ],
        'prefix_class' => 'elementor-grid%s-',
      ]
    );

    $repeater = new Repeater();

    $repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'elementor-gemcrypto' ),
        'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => __('Phase title', 'elementor-gemcrypto' ),
			]
		);

    $repeater->add_control(
      'list_phases',
      [
        'label' => __( 'Step List', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __(
          '<ul>
            <li class="active">This is step list 01</li>
            <li class="active">This is step list 02</li>
            <li>This is step list 03</li>
            <li>This is step list 04</li>
          </ul>',
          'elementor-gemcrypto'
        ),
				'placeholder' => __( 'Type your phase here', 'elementor-gemcrypto' ),
        'description' => esc_html__( 'Add class "active" in tag "li" like this <li class="active"> to active is checked in Bulleted List', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'list',
      [
        'label' => esc_html__( 'Phases', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_title' => __('Phase title #01', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #02', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #03', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #04', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #05', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #06', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #07', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #08', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
        ],
        'title_field' => '{{{ list_title }}}',
      ]
    );

    $this->end_controls_section();

    $this->start_controls_section(
      'button_section',
      [
        'label' => __( 'Load More', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_control(
      'show_more',
      [
        'label' => __( 'Show More', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SWITCHER,
				'description' => __( 'Number items in list is greater than init show number.', 'elementor-gemcrypto' ),
        'default' => '',
      ]
    );

		$this->add_control(
			'init_show',
			[
				'label' => __( 'Init Show', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'default' => 4,
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

    $this->add_control(
      'init_load', [
        'label' => __( 'Init Load', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'min' => 1,
        'default' => 4,
        'condition' => [
          'show_more' => 'yes'
        ],
      ]
    );

		$this->add_control(
			'show_more_text', [
				'label' => __( 'Show More Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Show More',
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

		$this->add_control(
			'show_less_text', [
				'label' => __( 'Show Less Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'Show Less',
				'condition' => [
					'show_more' => 'yes'
				],
			]
		);

    $this->end_controls_section();

  }

  protected function register_design_latyout_section_controls() {
    $this->start_controls_section(
      'design_layout_section',
      [
        'label' => __( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_control(
      'column_gap',
      [
        'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_control(
      'row_gap',
      [
        'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_control(
      'alignment',
      [
        'label' => __( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
        'options' => [
          'left' => [
            'title' => __( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => __( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => __( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'text-align: {{VALUE}};',
          '{{WRAPPER}} .eg-phase-item .eg-phase-content li' => 'justify-content: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'button_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'condition' => [
          'show_more' => 'yes',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-show-more' => 'margin-top: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->end_controls_section();

  }

  protected function register_design_phases_box_section_controls() {
    $this->start_controls_section(
			'phases_list_box_section',
			[
				'label' => esc_html__( 'Phase Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
      'phases_boder_width',
      [
        'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_radius',
      [
        'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => [ 'px', '%' ],
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    $this->add_responsive_control(
      'phases_padding',
      [
        'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'phases_box_shadow',
				'selector' => '{{WRAPPER}} .eg-phase-item',
			]
		);

    $this->start_controls_tabs( 'phases_effects_tabs' );

    $this->start_controls_tab( 'phases_tabs_normal',
      [
        'label' => __( 'Normal', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'phases_background_color',
      [
        'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'background-color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_color',
      [
        'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-color: {{VALUE}};',
        ],
      ]
    );

    $this->end_controls_tab();

    $this->start_controls_tab( 'phases_tabs_hover',
      [
        'label' => __( 'Hover', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'phases_background_color_hover',
      [
        'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item:hover' => 'background-color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_color_hover',
      [
        'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item:hover' => 'border-color: {{VALUE}};',
        ],
      ]
    );

    $this->end_controls_tab();

    $this->end_controls_tabs();

    $this->end_controls_section();

  }

  protected function register_design_phases_content_section_controls()
  {
    $this->start_controls_section(
      'phases_content_section',
      [
        'label' => esc_html__( 'Content', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_control(
      'phase_step_heading',
      [
        'label' => esc_html__( 'Phase', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_step_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-step',
      ]
    );

    $this->add_control(
      'phases_step_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-step' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_title_heading',
      [
        'label' => esc_html__( 'Title', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_title_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-title',
      ]
    );

    $this->add_control(
      'phases_title_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-title' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_step_list_heading',
      [
        'label' => esc_html__( 'List', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'phase_step_list_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content' => 'margin-top: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_step_list_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-content, {{WRAPPER}} .eg-phase-item .eg-phase-content ul li',
      ]
    );

    $this->add_control(
      'phases_step_list_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_icon_heading',
      [
        'label' => esc_html__( 'Bullet Icon', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'phase_icon_size',
      [
        'label' => esc_html__( 'Size', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content li:before' => 'min-width: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'phase_icon_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content li:before' => 'margin-right: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->end_controls_section();
  }

	protected function register_design_style_button_section_controls()
	{
		$this->start_controls_section(
			'section_design_show_more',
			[
				'label' => esc_html__( 'Show More', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_more' => 'yes',
				],
			]
		);

    $this->add_control(
      'alignment_show_more',
      [
        'label' => __( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
        'options' => [
          'left' => [
            'title' => __( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => __( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => __( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-show-more' => 'text-align: {{VALUE}};',
        ],
      ]
    );

		$this->add_control(
			'show_more_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-show-more--btn-view-',
			]
		);

		$this->add_control(
			'show_more_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'show_more_border',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'show_more_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'show_more_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-show-more a',
			]
		);

		$this->start_controls_tabs( 'tabs_show_more' );

		$this->start_controls_tab(
			'tab_show_more_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_show_more_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

  protected function register_controls() {
    $this->register_layout_section_controls();
    $this->register_design_latyout_section_controls();
    $this->register_design_phases_box_section_controls();
    $this->register_design_phases_content_section_controls();
    $this->register_design_style_button_section_controls();
  }

  public function render_element_header() {
		$settings = $this->get_settings_for_display();
		$classes = 'eg-phases';

		if( $settings['show_more'] === 'yes' ) {
			$classes .= ' has-show-more';
		}

    $this->add_render_attribute( 'wrapper', 'class', $classes );

	  ?>
      <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
    <?php
  }

  public function render_element_footer() {
    ?>
      </div>
    <?php
  }

	public function render_element_show_more() {
		$settings = $this->get_settings_for_display();
		if( $settings['show_more'] !== 'yes' || count( $settings['list'] ) <= $settings['init_show'] ) {
			return;
		}

		echo '<div class="eg-show-more" data-show="' . $settings['init_show'] . '" data-load="' . $settings['init_load'] . '" >
						<a class="eg-show-more-btn" href="#">' . $settings['show_more_text'] . '</a>
						<a class="eg-show-less-btn" href="#">' . $settings['show_less_text'] . '</a>
					</div>';
	}

  protected function render() {
    $settings = $this->get_settings_for_display();

    $this->render_element_header();

    ?>
    <div class="elementor-grid eg-list-phases" >
			<?php
      if( !empty( $settings['list'] ) ) {
        foreach ( $settings['list'] as $key => $value ) {
          ?>
          <div class="elementor-item eg-phase-item">
            <div class="eg-phase-heading">
              <div class="eg-phase-step">
                <?php echo "Phase ".($key + 1); ?>
              </div>
              <?php if (!empty($value['list_title'])): ?>
                <h3 class="eg-phase-title"><?php echo $value['list_title']; ?></h3>
              <?php endif; ?>
            </div>
            <div class="eg-phase-content">
              <?php if (!empty($value['list_phases'])): ?>
                <?php echo $value['list_phases']; ?>
              <?php endif; ?>
            </div>
          </div>
          <?php
        }
      }
      ?>
		</div>
    <?php

		$this->render_element_show_more();

    $this->render_element_footer();

  }

  protected function content_template() {

  }

}




 ?>
