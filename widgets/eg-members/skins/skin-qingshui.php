<?php
namespace ElementorGemcrypto\Widgets\Members\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Qingshui extends Skin_Base {

  protected function _register_controls_actions() {
    add_action( 'elementor/element/eg-members/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );
    add_action( 'elementor/element/eg-members/section_design_layout/before_section_end', [ $this, 'register_design_layout_section_controls' ] );
    add_action( 'elementor/element/eg-members/section_design_image/after_section_end', [ $this, 'register_design_content_section_controls' ] );


	}

	public function get_id() {
		return 'skin-qingshui';
	}

	public function get_title() {
		return __( 'Qingshui', 'elementor-gemcrypto' );
	}

  public function get_style_depends() {
		return [ 'eg-members', 'eg-show-more' ];
	}

  public function register_layout_section_controls( Widget_Base $widget ) {
    $this->parent = $widget;

    $this->add_responsive_control(
      'columns',
      [
        'label' => __( 'Columns', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => '3',
        'tablet_default' => '2',
        'mobile_default' => '1',
        'options' => [
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
          '5' => '5',
          '6' => '6',
        ],
        'prefix_class' => 'elementor-grid%s-',
      ]
    );

    $this->add_control(
      'posts_per_page',
      [
        'label' => __( 'Posts Per Page', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'default' => 3,
      ]
    );

    $this->add_group_control(
      Group_Control_Image_Size::get_type(),
      [
        'name' => 'thumbnail',
        'default' => 'medium',
        'exclude' => [ 'custom' ],
      ]
    );

    $this->add_responsive_control(
      'image_ratio',
      [
        'label' => __( 'Image Ratio', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 1.1,
        ],
        'range' => [
          'px' => [
            'min' => 0.3,
            'max' => 2,
            'step' => 0.01,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-member__featured' => 'padding-bottom: calc( {{SIZE}} * 100% );',
        ],
      ]
    );

  }

  public function register_design_layout_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;


    $this->add_control(
      'column_gap',
      [
        'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_control(
      'row_gap',
      [
        'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
          'size' => 30,
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
        ],
      ]
    );

    $this->add_responsive_control(
      'alignment',
      [
        'label' => __( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
        'options' => [
          'left' => [
            'title' => __( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => __( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => __( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-member' => 'text-align: {{VALUE}};',
        ],
      ]
    );


  }

  public function register_design_content_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__title' => 'color: {{VALUE}};',
				],
			]
		);

    $this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-member__title',
			]
		);

		$this->add_control(
			'heading_job_style',
			[
				'label' => __( 'Job', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'job_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__job' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'job_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-member__job',
			]
		);

    $this->add_control(
			'heading_email_style',
			[
				'label' => __( 'Email', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'email_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__email' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'email_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-member__email',
			]
		);

    $this->add_control(
			'heading_phone_number_style',
			[
				'label' => __( 'Phone Number', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'phone_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__phone-number' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'phone_number_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-member__phone-number',
			]
		);

		$this->add_control(
			'heading_social_style',
			[
				'label' => __( 'Social', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'social_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'stacked',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-members--social-view-',
			]
		);

		$this->add_control(
			'social_spacing',
			[
				'label' => __( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-member__socials a' => 'margin: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'social_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-member__socials a svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'social_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-member__socials a' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'condition' => [
					'social_view!' => '',
				],
			]
		);

		$this->add_control(
			'social_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'social_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'social_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'social_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-stacked .eg-member__socials a,
					 {{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_social' );

		$this->start_controls_tab(
			'tab_social_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'social_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__socials a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'social_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'social_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-stacked .eg-member__socials a' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'social_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'social_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_social_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'social_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-member__socials a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'social_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'social_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-stacked .eg-member__socials a:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'social_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'social_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-members--social-view-framed .eg-member__socials a:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

    $this->end_controls_section();

  }

  public function render_post()
  {

    $job = get_field('member_job');
    $email = get_field('member_email');
    $phone = get_field('member_phone');
		$socials = get_field('member_socials');

    ?>
    <article id="post-<?php the_ID();  ?>" <?php post_class( 'elementor-item eg-member' ); ?>>
      <div class="eg-member__thumbnail">
        <div class="eg-member__featured"><?php the_post_thumbnail( $this->parent->get_instance_value_skin('thumbnail_size') ); ?></div>
        <div class="eg-member__contact">
          <?php if ( !empty($email) || !empty($phone) ): ?>
            <div class="eg-member__phone-email">
              <?php if (!empty($phone)): ?>
                <a class="eg-member__phone-number" href="tel:<?php echo $phone; ?>">
                  <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone-alt fa-w-16 fa-3x">
                    <path fill="currentColor" d="M497.39 361.8l-112-48a24 24 0 0 0-28 6.9l-49.6 60.6A370.66 370.66 0 0 1 130.6 204.11l60.6-49.6a23.94 23.94 0 0 0 6.9-28l-48-112A24.16 24.16 0 0 0 122.6.61l-104 24A24 24 0 0 0 0 48c0 256.5 207.9 464 464 464a24 24 0 0 0 23.4-18.6l24-104a24.29 24.29 0 0 0-14.01-27.6z" class=""></path>
                  </svg>
                  <?php echo $phone; ?>
                </a>
              <?php endif; ?>
              <?php if (!empty($email)): ?>
                <a class="eg-member__email" href="mailto:<?php echo $email; ?>">
                  <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="envelope-open" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-envelope-open fa-w-16 fa-3x">
                    <path fill="currentColor" d="M349.32 52.26C328.278 35.495 292.938 0 256 0c-36.665 0-71.446 34.769-93.31 52.26-34.586 27.455-109.525 87.898-145.097 117.015A47.99 47.99 0 0 0 0 206.416V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V206.413a47.989 47.989 0 0 0-17.597-37.144C458.832 140.157 383.906 79.715 349.32 52.26zM464 480H48c-8.837 0-16-7.163-16-16V206.161c0-4.806 2.155-9.353 5.878-12.392C64.16 172.315 159.658 95.526 182.59 77.32 200.211 63.27 232.317 32 256 32c23.686 0 55.789 31.27 73.41 45.32 22.932 18.207 118.436 95.008 144.714 116.468a15.99 15.99 0 0 1 5.876 12.39V464c0 8.837-7.163 16-16 16zm-8.753-216.312c4.189 5.156 3.393 12.732-1.776 16.905-22.827 18.426-55.135 44.236-104.156 83.148-21.045 16.8-56.871 52.518-93.318 52.258-36.58.264-72.826-35.908-93.318-52.263-49.015-38.908-81.321-64.716-104.149-83.143-5.169-4.173-5.966-11.749-1.776-16.905l5.047-6.212c4.169-5.131 11.704-5.925 16.848-1.772 22.763 18.376 55.014 44.143 103.938 82.978 16.85 13.437 50.201 45.69 73.413 45.315 23.219.371 56.562-31.877 73.413-45.315 48.929-38.839 81.178-64.605 103.938-82.978 5.145-4.153 12.679-3.359 16.848 1.772l5.048 6.212z" class="">
                    </path>
                  </svg>
                  <?php echo $email; ?>
                </a>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php
          if( !empty( $socials ) ) {
            echo '<div class="eg-member__socials">';
              foreach ( $socials as $value ) {
                echo '<a class="eg-member__'.$value['social'].'" href="' . $value['link'] . '">';
                if( 'facebook' == $value['social'] ) {
                  echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-facebook-f"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" class=""></path></svg>';
                }

                if( 'twitter' == $value['social'] ) {
                  echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-twitter"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" class=""></path></svg>';
                }

                if( 'instagram' == $value['social'] ) {
                  echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-instagram"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" class=""></path></svg>';
                }

                if( 'linkedin' == $value['social'] ) {
                  echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z" class=""></path></svg>';
                }
                echo '</a>';
              }
            echo '</div>';
          }
          ?>
        </div>
      </div>

      <div class="eg-member__content">
        <?php
          the_title( '<h3 class="eg-member__title"><a href="' . get_the_permalink() . '">', '</a></h3>' );

          if( !empty( $job ) ) {
            echo '<div class="eg-member__job">' . $job . '</div>';
          }

        ?>
      </div>
    </article>
    <?php

  }

	public function render() {
    $query = $this->parent->query_posts();

		$this->parent->render_element_header();

			if ( $query->have_posts() ) {

				$this->parent->render_loop_header();

					while ( $query->have_posts() ) {
						$query->the_post();

						$this->render_post();
					}

				$this->parent->render_loop_footer();

				$this->parent->render_element_show_more();

			} else {
					echo '<div class="eg-no-posts-found">' . esc_html__('No posts found!', 'elementor-gemcrypto') . '</div>';
			}
			wp_reset_postdata();

		$this->parent->render_element_footer();

	}

	protected function content_template() {

	}
}
