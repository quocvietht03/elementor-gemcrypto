<?php
namespace ElementorGemcrypto\Widgets\Testimonials;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Css_Filter;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Testimonials extends Widget_Base {

	public function get_name() {
		return 'eg-testimonials';
	}

	public function get_title() {
		return __( 'EG Testimonials', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-testimonials', 'eg-show-more' ];
	}

	public function get_script_depends() {
		return ['eg-show-more'];
	}

	protected function _register_skins() {
		$this->add_skin( new Skins\Skin_Horizontal( $this ) );

	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

    $this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'prefix_class' => 'elementor-grid%s-',
				'condition' => [
					'_skin' => '',
				],
			]
		);



		$this->end_controls_section();
	}

	protected function register_data_layout_section_controls()
	{
		$this->start_controls_section(
			'section_data_layout',
			[
				'label' => __( 'Data Layout', 'elementor-gemcrypto' ),
			]
		);

		$repeater = new Repeater();

    $repeater->add_control(
			'list_image', [
				'label' => __( 'Thumbnail', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

    $repeater->add_control(
			'list_name', [
				'label' => __( 'Name', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
        'label_block' => true,
				'default' => '',
			]
		);

    $repeater->add_control(
			'list_job', [
				'label' => __( 'list_job', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
        'label_block' => true,
				'default' => '',
			]
		);

    $repeater->add_control(
			'list_desc', [
				'label' => __( 'Description', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXTAREA,
        'label_block' => true,
				'default' => '',
			]
		);

    $repeater->add_control(
      'list_rating_number',
      [
        'label' => esc_html__( 'Rating Number', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'min' => 0,
        'max' => 5,
        'step' => 0.1,
        'default' => 5,
      ]
    );

		$this->add_control(
			'list',
			[
				'label' => __( 'List', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
          [
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #01', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #01', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
					[
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #02', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #02', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #03', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #03', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #04', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #04', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #05', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #05', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
						'list_name' => __( 'Name #06', 'elementor-gemcrypto' ),
            'list_job' => __( 'Job #06', 'elementor-gemcrypto' ),
						'list_desc' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
            'list_rating_number' => 3.5,
					],
				],
        'title_field' => '{{{ list_name }}}',
			]
		);

		$this->end_controls_section();
	}

  protected function register_rating_stars_section_controls(){
		$this->start_controls_section(
			'section_rating_stars',
			[
				'label' => __( 'Rating Stars', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'star_style',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'star_fontawesome' => 'Font Awesome',
					'star_unicode' => 'Unicode',
				],
				'default' => 'star_fontawesome',
				'render_type' => 'template',
				'prefix_class' => 'elementor--star-style-',
			]
		);

		$this->add_control(
			'unmarked_star_style',
			[
				'label' => esc_html__( 'Unmarked Style', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'solid' => [
						'title' => esc_html__( 'Solid', 'elementor-gemcrypto' ),
						'icon' => 'eicon-star',
					],
					'outline' => [
						'title' => esc_html__( 'Outline', 'elementor-gemcrypto' ),
						'icon' => 'eicon-star-o',
					],
				],
				'default' => 'solid',
			]
		);

		$this->add_control(
			'icon_star_space',
			[
				'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-star-rating i:not(:last-of-type)' => 'margin-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_show_more_section_controls() {
		$this->start_controls_section(
			'section_show_more',
			[
				'label' => __( 'Show More', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more',
			[
				'label' => __( 'Show More', 'bearsthemes-addons' ),
				'description' => __( 'Number items in list is greater than init show number.', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'init_show', [
				'label' => __( 'Init Show', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 3,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'init_load', [
				'label' => __( 'Init Load', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 3,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_more_text',
			[
				'label' => __( 'Show More Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Show More', 'elementor-gemcrypto' ),
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_less_text',
			[
				'label' => __( 'Show Less Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Show Less', 'elementor-gemcrypto' ),
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_latyout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'column_gap',
			[
				'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'row_gap',
			[
				'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_box_section_controls() {
		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'border-radius: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'bg_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-testimonial',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-testimonial:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  protected function register_design_image_section_controls() {
		$this->start_controls_section(
			'section_design_image',
			[
				'label' => __( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-thumbnail' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'thumbnail_effects_tabs' );

		$this->start_controls_tab( 'normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_filters',
				'selector' => '{{WRAPPER}} .eg-testimonial .eg-thumbnail img',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_hover_filters',
				'selector' => '{{WRAPPER}} .eg-testimonial:hover .eg-thumbnail img',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
			'heading_desc_style',
			[
				'label' => __( 'Description', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-desc' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'desc_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-testimonial .eg-desc',
			]
		);

    $this->add_control(
			'heading_start_rating_style',
			[
				'label' => __( 'Start Rating', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'start_rating_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-rating  i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'start_rating_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-rating' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'start_rating_color_active',
			[
				'label' => __( 'Color Active', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-rating i:before' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'heading_name_style',
			[
				'label' => __( 'Name', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'name_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-name' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-testimonial .eg-name',
			]
		);

    $this->add_control(
			'heading_job_style',
			[
				'label' => __( 'Job', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'job_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-testimonial .eg-job' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'job_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-testimonial .eg-job',
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_show_more_section_controls() {
		$this->start_controls_section(
			'section_design_show_more',
			[
				'label' => __( 'Show More', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_more_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-show-more--btn-view-',
			]
		);

		$this->add_control(
			'show_more_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'show_more_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'show_more_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'show_more_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-show-more a',
			]
		);

		$this->start_controls_tabs( 'tabs_show_more' );

		$this->start_controls_tab(
			'tab_show_more_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_show_more_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();
		$this->register_data_layout_section_controls();
    $this->register_rating_stars_section_controls();
		$this->register_show_more_section_controls();

		$this->register_design_latyout_section_controls();
    $this->register_design_box_section_controls();
    $this->register_design_image_section_controls();
    $this->register_design_content_section_controls();
		$this->register_design_show_more_section_controls();
	}

	public function get_instance_value_skin( $key ) {
		$settings = $this->get_settings_for_display();

		if( !empty( $settings['_skin'] ) && isset( $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key] ) ) {
			return $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key];
		}

		if( isset( $settings[$key] ) ) {
			return $settings[$key];
		}

		return;
	}

	public function render_element_header() {
		$settings = $this->get_settings_for_display();

		$classes = 'eg-testimonials';

		if( !empty( $settings['_skin'] ) ) {
			$classes .= ' eg-testimonials--' . $settings['_skin'];
		} else {
			$classes .= ' eg-testimonials--skin-default';
		}

		if( $settings['show_more'] === 'yes' ) {
			$classes .= ' has-show-more';
		}

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		?>
			<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {

		?>
			</div>
		<?php
	}

  public function render_stars( $value ) {
		$settings = $this->get_settings_for_display();

    $rating_data = $value['list_rating_number'];
    $rating = (float) $rating_data;
    $floored_rating = floor( $rating );
    $stars_html = '';
    $icon = '&#xE934;';

		if ( 'star_fontawesome' === $settings['star_style'] ) {
			if ( 'outline' === $settings['unmarked_star_style'] ) {
				$icon = '&#xE933;';
			}
		} elseif ( 'star_unicode' === $settings['star_style'] ) {
			$icon = '&#9733;';

			if ( 'outline' === $settings['unmarked_star_style'] ) {
				$icon = '&#9734;';
			}
		}

    for ( $stars = 1.0; $stars <= 5; $stars++ ) {
      if ( $stars <= $floored_rating ) {
        $stars_html .= '<i class="elementor-star-full">' . $icon . '</i>';
      } elseif ( $floored_rating + 1 === $stars && $rating !== $floored_rating ) {
        $stars_html .= '<i class="elementor-star-' . ( $rating - $floored_rating ) * 10 . '">' . $icon . '</i>';
      } else {
        $stars_html .= '<i class="elementor-star-empty">' . $icon . '</i>';
      }
    }

    $stars_element = '<span class="elementor-star-rating eg-rating">' . $stars_html . '</span>';

    return $stars_element;
  }

	public function render_element_show_more() {
		$settings = $this->get_settings_for_display();
		if( $settings['show_more'] !== 'yes' || count( $settings['list'] ) <= $settings['init_show'] ) {
			return;
		}

		echo '<div class="eg-show-more" data-show="' . $settings['init_show'] . '" data-load="' . $settings['init_load'] . '" >
						<a class="eg-show-more-btn" href="#">' . $settings['show_more_text'] . '</a>
						<a class="eg-show-less-btn" href="#">' . $settings['show_less_text'] . '</a>
					</div>';
	}

	public function render_element_item( $value ) {
		$attachment = wp_get_attachment_image_src( $value['list_image']['id'], 'full' );
		$thumbnail = !empty( $attachment ) ? $attachment[0] : $value['list_image']['url'];

		echo '<div class="elementor-item eg-testimonial">';

			echo '<div class="eg-thumbnail-wrap">
              <div class="eg-thumbnail">
                <img src=" ' . esc_url( $thumbnail ) . ' " alt="">
              </div>
            </div>';

			echo '<div class="eg-content">';
        if( !empty( $value['list_desc'] ) ) {
          echo '<div class="eg-desc">' . $value['list_desc'] . '</div>';
        }

        if( !empty( $value['list_rating_number'] ) ) {
          echo $this->render_stars($value);
				}

				if( !empty( $value['list_name'] ) ) {
          echo '<h3 class="eg-name">' . $value['list_name'] . '</h3>';
				}

        if( !empty( $value['list_job'] ) ) {
          echo '<div class="eg-job">' . $value['list_job'] . '</div>';
				}

			echo '</div>';

		echo '</div>';
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		?>

		<div class="elementor-grid eg-list-testimonials">
			<?php
        if( !empty( $settings['list'] ) ) {
          foreach ( $settings['list'] as $key => $value ) {
            $this->render_element_item( $value );
          }
        }
      ?>
		</div>

		<?php

		$this->render_element_show_more();

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
