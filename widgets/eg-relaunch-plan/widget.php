<?php
namespace ElementorGemcrypto\Widgets\Relaunch_Plan;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Relaunch_Plan extends Widget_Base {

	public function get_name() {
		return 'eg-relaunch-plan';
	}

	public function get_title() {
		return __( 'EG Relaunch Plan', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-relaunch-plan' ];
	}

	public function get_script_depends() {
		return [];
	}

	protected function register_skins() {
		$this->add_skin( new Skins\Skin_Vertical( $this ) );

	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

    $repeater = new Repeater();

		$repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
        'label_block' => true,
				'default' => '',
			]
		);

		$repeater->add_control(
			'list_content', [
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::WYSIWYG,
        'label_block' => true,
				'default' => '',
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Phases', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'list_title' => __( 'Project Relaunch', 'elementor-gemcrypto' ),
						'list_content' => '',
					],
					[
            'list_title' => __( 'Product Development', 'elementor-gemcrypto' ),
						'list_content' => '',
					],
					[
            'list_title' => __( 'Project Maturity', 'elementor-gemcrypto' ),
						'list_content' => '',
					],
					[
            'list_title' => __( 'Product Expansion', 'elementor-gemcrypto' ),
						'list_content' => '',
					],
				],
        'title_field' => '{{{ list_title }}}',
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_phases_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_count_style',
			[
				'label' => __( 'Count', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'count_color',
			[
				'label' => __( 'Count Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-phase .eg-count' => 'color: {{VALUE}};',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_count_text',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-phase .eg-count',
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_title',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-title',
			]
		);

    $this->add_control(
			'heading_content_style',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'content_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-content' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_content',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-content',
			]
		);

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_phases_section_controls();
	}

	public function get_instance_value_skin( $key ) {
		$settings = $this->get_settings_for_display();

		if( !empty( $settings['_skin'] ) && isset( $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key] ) ) {
			 return $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key];
		}
		return $settings[$key];
	}

	public function render_element_header() {
		$settings = $this->get_settings_for_display();

		$classes = 'eg-relaunch-plan';

		if( !empty( $settings['_skin'] ) ) {
			$classes .= ' eg-relaunch-plan--' . $settings['_skin'];
		} else {
			$classes .= ' eg-relaunch-plan--skin-default';
		}

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		?>
			<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {

		?>
			</div>
		<?php
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		?>

		<div class="eg-phases">
			<?php
        if( !empty( $settings['list'] ) ) {
          foreach ( $settings['list'] as $key => $value ) {
            $key++;
            ?>
              <div class="eg-phase">
                <?php
                  echo '<div class="eg-count">' . __( 'Phase ', 'elementor-gemcrypto' ) . $key . '</div>';
                  if( !empty( $value['list_title'] ) ) {
                    echo '<h3 class="eg-title">' . $value['list_title'] . '</h3>';
                  }

                  if( !empty( $value['list_content'] ) ) {
                    echo '<div class="eg-content">' . $value['list_content'] . '</div>';
                  }
                ?>
              </div>
            <?php
          }
        }
      ?>
		</div>

		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
