<?php
namespace ElementorGemcrypto\Widgets\Relaunch_Plan\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Vertical extends Skin_Base {

  protected function _register_controls_actions() {
		add_action( 'elementor/element/eg-relaunch-plan/section_layout/before_section_end', [ $this, 'register_layout_section_controls' ] );
    add_action( 'elementor/element/eg-relaunch-plan/section_design_content/before_section_start', [ $this, 'register_design_line_section_controls' ] );
    add_action( 'elementor/element/eg-relaunch-plan/section_design_content/before_section_end', [ $this, 'register_design_phases_section_controls' ] );

	}

	public function get_id() {
		return 'skin-vertical';
	}

	public function get_title() {
		return __( 'Vertical', 'elementor-gemcrypto' );
	}

  public function register_layout_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'list',
      ]
    );

    $this->add_control(
			'start_label',
			[
				'label' => __( 'Start Label', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Start', 'elementor-gemcrypto' ),
			]
		);

    $this->add_control(
			'end_label',
			[
				'label' => __( 'End Label', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'End', 'elementor-gemcrypto' ),
			]
		);

    $this->parent->end_injection();

	}

  public function register_design_line_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

		$this->start_controls_section(
			'section_design_line',
			[
				'label' => __( 'Line', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
			'heading_label_style',
			[
				'label' => __( 'Label', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'label_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-label' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_lable',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-label',
			]
		);

    $this->add_control(
			'heading_line_style',
			[
				'label' => __( 'Line', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

    $this->add_control(
			'line_color',
			[
				'label' => __( 'Line Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-line,
           {{WRAPPER}} .eg-phases .eg-phase:after' => 'background-color: {{VALUE}};',
          '{{WRAPPER}} .eg-phases .eg-phase:before' => 'border-color: {{VALUE}};',
				],
			]
		);

    $this->add_control(
			'line_text_color',
			[
				'label' => __( 'Text Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-phase .eg-count' => 'color: {{VALUE}};',
          '{{WRAPPER}} .eg-phases .eg-phase:before' => 'background-color: {{VALUE}};',
				],
			]
		);

    $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_line_text',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-phase .eg-count',
			]
		);

		$this->end_controls_section();
	}

  public function register_design_phases_section_controls( Widget_Base $widget ) {
		$this->parent = $widget;

    $this->parent->update_control(
      'heading_count_style',
			[
        'condition' => [
					'_skin' => '',
				],
			]
		);

    $this->parent->update_control(
      'count_color',
			[
        'condition' => [
					'_skin' => '',
				],
			]
		);

    $this->parent->update_control(
      'typography_count_text_typography',
			[
        'condition' => [
					'_skin' => '',
				],
			]
		);

  }

	public function render() {
    $settings = $this->parent->get_settings_for_display();

		$this->parent->render_element_header();

		?>
    <div class="eg-line">
      <?php
        if( $this->parent->get_instance_value_skin('start_label') ) {
          echo '<span class="eg-label eg-start-label">' . $this->parent->get_instance_value_skin('start_label') . '</span>';
        }

        if( $this->parent->get_instance_value_skin('end_label') ) {
          echo '<span class="eg-label eg-end-label">' . $this->parent->get_instance_value_skin('end_label') . '</span>';
        }
      ?>
    </div>

		<div class="eg-phases">
			<?php
        if( !empty( $settings['list'] ) ) {
          foreach ( $settings['list'] as $key => $value ) {
            $key++;
            ?>
              <div class="eg-phase">
                <?php
                  echo '<div class="eg-count">' . __( 'Phase ', 'elementor-gemcrypto' ) . $key . '</div>';
                  if( !empty( $value['list_title'] ) ) {
                    echo '<h3 class="eg-title">' . $value['list_title'] . '</h3>';
                  }

                  if( !empty( $value['list_content'] ) ) {
                    echo '<div class="eg-content">' . $value['list_content'] . '</div>';
                  }
                ?>
              </div>
            <?php
          }
        }
      ?>
		</div>

		<?php

		$this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
