<?php
namespace ElementorGemcrypto\Widgets\Language_Switcher;


use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Utils;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Language_Switcher extends Widget_Base {

	public function get_name() {
		return 'eg-language-switcher';
	}

	public function get_title() {
		return __( 'EG Language Switcher', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-language-switcher' ];
	}

	public function get_script_depends() {
		return ['eg-language-switcher'];
	}

  public function register_layout_section_controls() {
    $this->start_controls_section(
      'section_layout',
      [
        'label' => __( 'List', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

		$this->add_control(
			'view',
			[
				'label' => __('View', 'elementor-gemcrypto'),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __('Default', 'elementor-gemcrypto'),
					'flags' => __('Flags', 'elementor-gemcrypto'),
					'hide-label' => __('Hide Label', 'elementor-gemcrypto'),
				],
				'prefix_class' => 'eg-language-switcher--view-',
			]
		);

    $repeater = new Repeater();

		$repeater->add_control(
			'image', [
				'label' => __( 'Flags', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$repeater->add_control(
			'label',
			[
				'label' => esc_html__( 'Name', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __('Language', 'elementor-gemcrypto'),
			]
		);

    $repeater->add_control(
      'link',
      [
        'label' => esc_html__( 'Link', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXT,
        'label_block' => true,
        'default' => __('', 'elementor-gemcrypto'),
        'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'list',
      [
        'label' => esc_html__( 'Language', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
						'image' => [
							'url' => plugins_url( '/eg-language-switcher/images/united-kingdom-flag.svg', __DIR__ ),
						],
						'label' => __('English', 'elementor-gemcrypto'),
						'link' => __('#','elementor-gemcrypto'),
          ],
					[
						'image' => [
							'url' => plugins_url( '/eg-language-switcher/images/germany-flag.svg', __DIR__ ),
						],
						'label' => __('Deutsch', 'elementor-gemcrypto'),
						'link' => __('#', 'elementor-gemcrypto'),
					],
          [
						'image' => [
							'url' => plugins_url( '/eg-language-switcher/images/france-flag.svg', __DIR__ ),
						],
						'label' => __('Français', 'elementor-gemcrypto'),
						'link' => __('#', 'elementor-gemcrypto'),
          ],
					[
						'image' => [
							'url' => plugins_url( '/eg-language-switcher/images/italy-flag.svg', __DIR__ ),
						],
						'label' => __('Italiano', 'elementor-gemcrypto'),
						'link' => __('#', 'elementor-gemcrypto'),
					],
        ],
        'title_field' => '{{{ label }}}',
      ]
    );

    $this->end_controls_section();

  }

  public function register_design_layout_section_controls() {

    $this->start_controls_section(
      'section_design',
      [
        'label' => esc_html__( 'Select', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_responsive_control(
      'language_switcher_align',
      [
        'label' => esc_html__( 'Alignment', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::CHOOSE,
				'default' => '',
        'options' => [
          'left' => [
            'title' => esc_html__( 'Left', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-left',
          ],
          'center' => [
            'title' => esc_html__( 'Center', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-center',
          ],
          'right' => [
            'title' => esc_html__( 'Right', 'elementor-gemcrypto' ),
            'icon' => 'eicon-text-align-right',
          ],
        ],
        'selectors' => [
					'{{WRAPPER}} .eg-select-layout' => 'text-align: {{VALUE}}',
				]
      ]
    );

		$this->add_control(
			'language_switcher_flag_size',
			[
				'label' => esc_html__( 'Flag Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-active .eg-selected::before' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-active .eg-selected img' => 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-active .eg-selected svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'label_typography',
				'default' => '',
				'condition' => [
					'view!' => 'hide-label',
				],
				'selector' => '{{WRAPPER}} .eg-select-active',
			]
		);

		$this->add_control(
			'label_color',
			[
				'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-select-active .eg-selected' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-select-active .eg-arrow' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'language_switcher_space',
			[
				'label' => esc_html__( 'Icon Toggle Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-box .eg-arrow' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

    $this->end_controls_section();
  }

	public function register_design_drop_box_section_controls()
	{
		$this->start_controls_section(
			'section_design_dropdown',
			[
				'label' => esc_html__( 'Dropdown', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'dropdown_flag_size',
			[
				'label' => esc_html__( 'Flag Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option img' => 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-list .eg-select-option svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'dropdown_label_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-select-list .eg-select-option .eg-option',
			]
		);

		$this->add_control(
			'dropdown_space',
			[
				'label' => esc_html__( 'Offset Top', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-list' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'dropdown_border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option:not(:last-child)' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-list .eg-select-option:last-child' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'dropdown_border_color',
			[
				'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'dropdown_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => ['px','%'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					]
				],
				'default' => [
					'unit' => 'px',
					'size' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-list' => 'border-radius: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-list .eg-select-option:first-child' => 'border-top-left-radius: {{SIZE}}{{UNIT}}; border-top-right-radius: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-select-list .eg-select-option:last-child' => 'border-bottom-left-radius: {{SIZE}}{{UNIT}}; border-bottom-right-radius: {{SIZE}}{{UNIT}};',
				]
			]
		);

		$this->add_control(
			'dropdown_vertical_padding',
			[
				'label' => esc_html__( 'Vertical Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'min' => 0,
					'max' => 50,
				],
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option' => 'padding-top: {{SIZE}}{{UNIT}};padding-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'dropdown_box_shadow',
				'selector' => '{{WRAPPER}} .eg-select-list',
			]
		);

		$this->start_controls_tabs( 'dropdown_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'dropdown_label_color',
			[
				'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option:not(.selected) .eg-option' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'dropdown_bg',
			[
				'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} li.eg-select-option' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'dropdown_label_color_hover',
			[
				'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option .eg-option:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'dropdown_bg_hover',
			[
				'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} li.eg-select-option:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_active',
			[
				'label' => __( 'Active', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'dropdown_label_color_active',
			[
				'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-select-list .eg-select-option.selected .eg-option' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'dropdown_bg_active',
			[
				'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} li.eg-select-option.selected' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  protected function register_controls() {
    $this->register_layout_section_controls();
    $this->register_design_layout_section_controls();
		$this->register_design_drop_box_section_controls();
  }

  public function render_element_header() {

    $this->add_render_attribute( 'wrapper', 'class', 'eg-select-layout' );
    ?>
      <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
    <?php
  }

  public function render_element_footer() {
    ?>
      </div>
    <?php
  }


  protected function render() {
    $settings = $this->get_settings_for_display();

    $this->render_element_header();

		$first_item = $settings['list'][0];

    ?>
    <div class="eg-select-box">
			<div class="eg-select-active">
				<div class="eg-selected">

				</div>
				<span class="eg-arrow">
					<i aria-hidden="true" class="fas fa-chevron-down"></i>
				</span>
			</div>
      <ul class="eg-select-list">
      <?php
      foreach ( $settings['list'] as $index => $item ) :
				$attachment = wp_get_attachment_image_src( $item['image']['id'], 'full' );
				$thumbnail = !empty( $attachment ) ? $attachment[0] : $item['image']['url'];

  			if (  ! empty( $item['image'] ) || ! empty( $item['label'] ) ) {
	        ?>
	        <li class="eg-select-option" data-index="<?php echo $index; ?>">
						<a href="<?php echo $item['link'] ; ?>">
							<div class="eg-option">
								<?php
								if ( !empty($thumbnail) ) {
									echo '<img src="' . esc_url( $thumbnail ) . '" alt="">';
								}
								if ( !empty($item['label']) ) {
									echo  '<span class="label">'.$item['label'].'</span>';
								}
								?>
							</div>
						</a>
	        </li>
	        <?php
  			}
      endforeach;
      ?>
	    </ul>
	  </div>
    <?php

    $this->render_element_footer();

  }

  protected function content_template() {

  }

}




 ?>
