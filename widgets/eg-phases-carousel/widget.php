<?php
namespace ElementorGemcrypto\Widgets\Phases_Carousel;


use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;



if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Phases_Carousel extends Widget_Base {

	public function get_name() {
		return 'eg-phases-carousel';
	}

	public function get_title() {
		return __( 'EG Phases Carousel', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'font-awesome', 'eg-widget-carousel','eg-phases' ];
	}

	public function get_script_depends() {
		return ['eg-swiper','eg-widget-carousel'];
	}

  protected function register_layout_section_controls() {
    $this->start_controls_section(
      'phases_layout_section',
      [
        'label' => __( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_responsive_control(
      'sliders_per_view',
      [
        'label' => __( 'Slides Per View', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => '4',
        'tablet_default' => '2',
        'mobile_default' => '1',
        'options' => [
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
          '5' => '5',
          '6' => '6',
        ],

      ]
    );

    $this->add_control(
      'sliders_per_column',
      [
        'label' => __( 'Slides Per Column', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => '1',
        'options' => [
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
        ],
      ]
    );

    $repeater = new Repeater();

    $repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'elementor-gemcrypto' ),
        'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'default' => __('Phase title', 'elementor-gemcrypto' ),
			]
		);

    $repeater->add_control(
      'list_phases',
      [
        'label' => __( 'Step List', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __(
          '<ul>
            <li class="active">This is step list 01</li>
            <li class="active">This is step list 02</li>
            <li>This is step list 03</li>
            <li>This is step list 04</li>
          </ul>',
          'elementor-gemcrypto'
        ),
				'placeholder' => __( 'Type your phase here', 'elementor-gemcrypto' ),
        'description' => esc_html__( 'Add class "active" in tag "li" like this <li class="active"> to active is checked in Bulleted List', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'list',
      [
        'label' => esc_html__( 'Phases', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_title' => __('Phase title #01', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #02', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #03', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #04', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #05', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #06', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #07', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
          [
            'list_title' => __('Phase title #08', 'elementor-gemcrypto'),
            'list_phases' => __(
              '<ul>
                <li class="active">This is step list 01</li>
                <li class="active">This is step list 02</li>
                <li>This is step list 03</li>
                <li>This is step list 04</li>
              </ul>',
              'elementor-gemcrypto'
            ),
          ],
        ],
        'title_field' => '{{{ list_title }}}',
      ]
    );

    $this->end_controls_section();

  }

  protected function register_additional_section_controls() {
		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'bearsthemes-addons' ),
			]
		);

    $this->add_control(
			'navigation',
			[
				'label' => __( 'Prev & Next Button', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'nav_btn_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'navigation!' => '',
				],
				'prefix_class' => 'eg-swiper-button--view-',
			]
		);

    $this->add_control(
			'pagination',
			[
				'label' => __( 'Dots', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Transition Duration', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'loop',
			[
				'label' => __( 'Infinite Loop', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_latyout_section_controls() {
    $this->start_controls_section(
      'design_layout_section',
      [
        'label' => __( 'Layout', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_responsive_control(
      'space_between',
      [
        'label' => __( 'Space Between', 'bearsthemes-addons' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'default' => [
          'size' => 30,
        ],
      ]
    );

    $this->add_responsive_control(
      'items_space_between',
      [
        'label' => __( 'Items Space Between', 'bearsthemes-addons' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'default' => [
          'size' => 30,
        ],
        'condition' => [
          'sliders_per_column!' => '1',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item:not(:first-child)' => 'margin-top: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->end_controls_section();

  }

  protected function register_design_phases_box_section_controls() {
    $this->start_controls_section(
			'phases_list_box_section',
			[
				'label' => esc_html__( 'Phase Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
      'phases_boder_width',
      [
        'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_radius',
      [
        'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => [ 'px', '%' ],
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    $this->add_responsive_control(
      'phases_padding',
      [
        'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'phases_box_shadow',
				'selector' => '{{WRAPPER}} .eg-phase-item',
			]
		);

    $this->start_controls_tabs( 'phases_effects_tabs' );

    $this->start_controls_tab( 'phases_tabs_normal',
      [
        'label' => __( 'Normal', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'phases_background_color',
      [
        'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'background-color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_color',
      [
        'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item' => 'border-color: {{VALUE}};',
        ],
      ]
    );

    $this->end_controls_tab();

    $this->start_controls_tab( 'phases_tabs_hover',
      [
        'label' => __( 'Hover', 'elementor-gemcrypto' ),
      ]
    );

    $this->add_control(
      'phases_background_color_hover',
      [
        'label' => esc_html__( 'Background Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item:hover' => 'background-color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phases_border_color_hover',
      [
        'label' => esc_html__( 'Border Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item:hover' => 'border-color: {{VALUE}};',
        ],
      ]
    );

    $this->end_controls_tab();

    $this->end_controls_tabs();

    $this->end_controls_section();

  }

  protected function register_design_phases_content_section_controls()
  {
    $this->start_controls_section(
      'phases_content_section',
      [
        'label' => esc_html__( 'Content', 'elementor-gemcrypto' ),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    $this->add_control(
      'phase_step_heading',
      [
        'label' => esc_html__( 'Phase', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_step_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-step',
      ]
    );

    $this->add_control(
      'phases_step_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-step' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_title_heading',
      [
        'label' => esc_html__( 'Title', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_title_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-title',
      ]
    );

    $this->add_control(
      'phases_title_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-heading .eg-phase-title' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_step_list_heading',
      [
        'label' => esc_html__( 'List', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'phase_step_list_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content' => 'margin-top: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'phase_step_list_typography',
        'selector' => '{{WRAPPER}} .eg-phase-item .eg-phase-content, {{WRAPPER}} .eg-phase-item .eg-phase-content ul li',
      ]
    );

    $this->add_control(
      'phases_step_list_color',
      [
        'label' => esc_html__( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content, {{WRAPPER}} .eg-phase-item .eg-phase-content ul li' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'phase_icon_heading',
      [
        'label' => esc_html__( 'Bullet Icon', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::HEADING,
      ]
    );

    $this->add_control(
      'phase_icon_size',
      [
        'label' => esc_html__( 'Size', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content li:before' => 'min-width: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'phase_icon_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-phase-item .eg-phase-content li:before' => 'margin-right: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->end_controls_section();
  }

  protected function register_design_navigation_section_controls() {
		$this->start_controls_section(
			'section_design_navigation',
			[
				'label' => __( 'Prev & Next Button', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation!' => '',
				]
			]
		);

		$this->add_responsive_control(
			'arrows_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-swiper-button svg' => 'width: {{SIZE}}{{UNIT}}; height: auto;',
				],
			]
		);

		$this->add_control(
			'icon_border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view' => 'framed',
				],
			]
		);

		$this->add_control(
			'icon_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view!' => '',
				],
			]
		);

		$this->add_responsive_control(
			'icon_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'padding-top: {{TOP}}{{UNIT}}; padding-right: {{RIGHT}}{{UNIT}}; padding-bottom: {{BOTTOM}}{{UNIT}}; padding-left: {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'nav_btn_view!' => '',
				],
			]
		);

		$this->start_controls_tabs( 'navigation_style' );

		$this->start_controls_tab(
			'navigation_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'navigation_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'navigation_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_pagination_section_controls() {
		$this->start_controls_section(
			'section_design_pagination',
			[
				'label' => __( 'Dots', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'pagination!' => '',
				]
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'pagination_space_between',
			[
				'label' => esc_html__( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'margin-left: {{SIZE}}{{UNIT}}; margin-right: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
			]
		);

		$this->start_controls_tabs( 'pagination_style' );

		$this->start_controls_tab(
			'pagination_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet:not(.swiper-pagination-bullet-active)' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'pagination_active',
			[
				'label' => __( 'Active', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color_active',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  protected function register_controls() {
    $this->register_layout_section_controls();
    $this->register_additional_section_controls();

    $this->register_design_latyout_section_controls();
    $this->register_design_phases_box_section_controls();
    $this->register_design_phases_content_section_controls();
    $this->register_design_navigation_section_controls();
    $this->register_design_pagination_section_controls();
  }

  protected function swiper_data() {
		$settings = $this->get_settings_for_display();
		$widget_id = $this->get_id();

		$slides_per_view = $settings['sliders_per_view'];
		$slides_per_view_tablet = isset( $settings['sliders_per_view_tablet'] ) ? $settings['sliders_per_view_tablet'] : 2;
		$slides_per_view_mobile = isset( $settings['sliders_per_view_mobile'] ) ? $settings['sliders_per_view_mobile'] : 1;

		$space_between = ( '' !== $settings['space_between']['size'] ) ? $settings['space_between']['size'] : 30;
		$space_between_tablet = ( isset( $settings['space_between_tablet']['size'] ) && '' !== $settings['space_between_tablet']['size'] ) ? $settings['space_between_tablet']['size'] : $space_between;
		$space_between_mobile = ( isset( $settings['space_between_mobile']['size'] ) && '' !== $settings['space_between_mobile']['size'] ) ? $settings['space_between_mobile']['size'] : $space_between_tablet;

		$swiper_data = array(
			'slidesPerView' => $slides_per_view_mobile,
			'spaceBetween' => $space_between_mobile,
			'speed' => $settings['speed'],
			'loop' => $settings['loop'] == 'yes' ? true : false,
			'breakpoints' => array(
				767 => array(
				  'slidesPerView' => $slides_per_view_tablet,
				  'spaceBetween' => $space_between_tablet,
				),
				1025 => array(
				  'slidesPerView' => $slides_per_view,
				  'spaceBetween' => $space_between,
				)
			),

		);

		if( $settings['navigation'] === 'yes' ) {
			$swiper_data['navigation'] = array(
				'nextEl' => '.eg-swiper-button-next-'.$widget_id,
				'prevEl' => '.eg-swiper-button-prev-'.$widget_id,
			);
		}

		if( $settings['pagination'] === 'yes' ) {
			$swiper_data['pagination'] = array(
				'el' => '.eg-swiper-pagination-'.$widget_id,
				'type' => 'bullets',
				'clickable' => true,
			);
		}

		if( $settings['autoplay'] === 'yes' ) {
			$swiper_data['autoplay'] = array(
				'delay' => $settings['autoplay_speed'],
			);
		}

		return $swiper_json = json_encode($swiper_data);
	}

  public function render_element_header() {
		$settings = $this->get_settings_for_display();
		$classes = 'swiper-container eg-phases';

    if( $settings['navigation'] === 'yes' ) {
      $classes .= ' has-navigation';
    }

    if( $settings['pagination'] === 'yes' ) {
      $classes .= ' has-pagination';
    }

    $this->add_render_attribute( 'wrapper', 'class', $classes );

    $this->add_render_attribute( 'wrapper', 'data-swiper', $this->swiper_data() );
	  ?>
      <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
    <?php
  }

  public function render_element_footer() {
    $settings = $this->get_settings_for_display();
    $widget_id = $this->get_id();
    ?>
      </div>
    <?php
    if( $settings['navigation'] === 'yes' ) {

      $select_icon_pre = 'select_icon_prev';
      $select_icon_next = 'select_icon_next';

      ?>
      <div class="eg-swiper-button eg-swiper-button-prev eg-swiper-button-prev-<?php echo $widget_id; ?>">
        <i class="fa fa-chevron-left"></i>
      </div>
      <div class="eg-swiper-button eg-swiper-button-next eg-swiper-button-next-<?php echo $widget_id; ?>">
        <i class="fa fa-chevron-right"></i>
      </div>
      <?php
    }

    if( $settings['pagination'] === 'yes' ) {
      echo '<div class="eg-swiper-pagination eg-swiper-pagination-'.$widget_id.'"></div>';
    }
  }

  protected function render() {
    $settings = $this->get_settings_for_display();

    $this->render_element_header();

    ?>
    <div class="swiper-wrapper eg-list-phases" >
			<?php
      if( !empty( $settings['list'] ) ) {
        $total = count($settings['list']);
        $count = 0;
        foreach ( $settings['list'] as $key => $value ) {
          $count++;
          ?>
          <?php if ( $count== 1): ?>
          <div class="swiper-slide">
          <?php endif; ?>
            <div class="eg-phase-item">
              <div class="eg-phase-heading">
                <div class="eg-phase-step">
                  <?php echo "Phase ".($key + 1); ?>
                </div>
                <?php if (!empty($value['list_title'])): ?>
                  <h3 class="eg-phase-title"><?php echo $value['list_title']; ?></h3>
                <?php endif; ?>
              </div>
              <div class="eg-phase-content">
                <?php if (!empty($value['list_phases'])): ?>
                  <?php echo $value['list_phases']; ?>
                <?php endif; ?>
              </div>
            </div>
          <?php if ($count == $settings['sliders_per_column'] || $total == $key): ?>
          </div>
          <?php endif; ?>
          <?php
          if( $count == $settings['sliders_per_column'] ) $count = 0;
        }
      }
      ?>
		</div>
    <?php

    $this->render_element_footer();

  }

  protected function content_template() {

  }

}




 ?>
