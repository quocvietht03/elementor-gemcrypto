<?php
namespace ElementorGemcrypto\Widgets\Scrolling_List;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Scrolling_List extends Widget_Base {

	public function get_name() {
		return 'eg-scrolling-list';
	}

	public function get_title() {
		return __( 'EG Scrolling List', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-scrolling-list' ];
	}

	public function get_script_depends() {
		return ['eg-marquee','eg-scrolling-list'];
	}

	public function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Text', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

    $this->add_control(
      'text',
      [
        'label' => esc_html__( 'Text', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
        'default' => __("There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.", 'elementor-gemcrypto' ),
      ]
    );

		$this->end_controls_section();

	}

  public function register_additional_options_section_controls()
  {
    $this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

    $this->add_control(
      'direction',
      [
        'label' => __( 'Direction', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => 'left',
        'options' => [
          'left' => __( 'Left', 'elementor-gemcrypto' ),
          'right' => __( 'Right', 'elementor-gemcrypto' ),
        ],
      ]
    );

    $this->add_control(
      'duration',
      [
        'label' => __( 'Transition Duration', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'default' => 10000,
      ]
    );

    $this->end_controls_section();
  }

	public function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_text',
			[
				'label' => __( 'Text', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_text',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-scroll-text',
			]
		);

    $this->add_control(
      'text_color',
      [
        'label' => __( 'Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-scroll-text' => 'color: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      'text_stroke_width',
      [
        'label' => __( 'Stroke Width', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-scroll-text' => '-webkit-text-stroke: {{SIZE}}{{UNIT}}; stroke-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'text_stroke_color',
      [
        'label' => __( 'Stroke Color', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'selectors' => [
          '{{WRAPPER}} .eg-scroll-text' => '-webkit-text-stroke-color: {{VALUE}}; stroke: {{VALUE}};',
        ],
      ]
    );

		$this->end_controls_section();
	}

	public function register_controls() {
		$this->register_layout_section_controls();
    $this->register_additional_options_section_controls();

		$this->register_design_content_section_controls();
	}

  public function marquee_data()
  {
    $settings = $this->get_settings_for_display();

    $direction = isset( $settings['direction'] ) ? $settings['direction'] : 'left';
    $duration = isset( $settings['duration'] ) ? $settings['duration'] : 10000;

    $marquee_data = array(
      'direction'=> $direction,
      'duration'=> $duration,
      'delayBeforeStart'=> 0,
      'duplicated'=> true,
      'startVisible'=> true,
    );

    return $marquee_json = json_encode($marquee_data);
  }

	public function render_element_header() {

		$this->add_render_attribute( 'wrapper', 'class', 'eg-scrolling-list' );

    $this->add_render_attribute( 'wrapper', 'data-marquee', $this->marquee_data() );

  	?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php

	}

	public function render_element_footer() {
		?>
			</div>
		<?php
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		?>
		<?php if (!empty($settings['text'])): ?>
      <div class="eg-scroll-text">
        <?php echo $settings['text']; ?>
      </div>
		<?php endif; ?>

		<?php
		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
