<?php
namespace ElementorGemcrypto\Widgets\Button_Copy;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Button_Copy extends Widget_Base {

	public function get_name() {
		return 'eg-button-copy';
	}

	public function get_title() {
		return __( 'EG Button Copy', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-button-copy' ];
	}

	public function get_script_depends() {
		return ['eg-button-copy'];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'select_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fa fa-clone',
					'library' => 'fa-solid',
				],
			]
		);

		$this->add_control(
			'icon_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-button-copy--icon-view-',
			]
		);

		$this->add_control(
			'icon_shape',
			[
				'label' => __( 'Shape', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'circle',
				'options' => [
					'circle' => __( 'Circle', 'elementor-gemcrypto' ),
					'square' => __( 'Square', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_view!' => '',
				],
				'prefix_class' => 'eg-button-copy--icon-shape-',
			]
		);

		$this->add_control(
			'text_copy',
			[
				'label' => __( 'Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'This is your text', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'text_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'full',
				'options' => [
					'full' => __( 'Full Text', 'elementor-gemcrypto' ),
					'short' => __( 'Short Text', 'elementor-gemcrypto' ),
				],
			]
		);

		$this->add_control(
			'text_excerpt_left',
			[
				'label' => __( 'Excerpt Left', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 0,
				'default' => 7,
				'condition' => [
					'text_view' => 'short',
				]
			]
		);

		$this->add_control(
			'text_excerpt_right',
			[
				'label' => __( 'Excerpt Right', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 0,
				'default' => 4,
				'condition' => [
					'text_view' => 'short',
				]
			]
		);


		$this->end_controls_section();
	}

	protected function register_design_layout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
        'default' => 'center',
				'options' => [
					'flex-start' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'flex-end' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-button-copy-box' => 'justify-content: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_icon_section_controls() {
		$this->start_controls_section(
			'section_design_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'icon_size',
			[
				'label' => __( 'Icon Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 300,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-icon svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

    $this->add_control(
      'icon_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-button-copy-wrap .eg-button-copy' => 'margin-left: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

		$this->add_control(
      'icon_padding',
      [
        'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-button-copy-box .eg-button-copy-wrap .eg-icon' => 'padding: {{SIZE}}{{UNIT}};',
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'condition' => [
          'icon_view!' => '',
        ],
      ]
    );

    $this->add_control(
      'border_radius',
      [
        'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => [ 'px', '%' ],
        'selectors' => [
          '{{WRAPPER}} .eg-button-copy-box .eg-button-copy-wrap .eg-icon' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
        'condition' => [
          'icon_view!' => '',
        ],
      ]
    );

		$this->add_control(
			'border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy-wrap .eg-icon' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'icon_view' => 'framed',
				],
			]
		);

    $this->add_group_control(
    	Group_Control_Box_Shadow::get_type(),
      [
        'name' => 'box_shadow',
        'label' => __( 'Box Shadow', 'elementor-gemcrypto' ),
        'condition' => [
          'icon_view!' => '',
        ],
        'selector' => '{{WRAPPER}} .eg-button-copy .eg-icon',
      ]
    );

		$this->start_controls_tabs( 'tabs_icon' );

		$this->start_controls_tab(
			'tab_icon_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy-wrap .eg-icon' => 'border-color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-icon' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-icon' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy-wrap .eg-icon' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_icon_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-button-copy:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-button-copy .eg-icon:hover svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-button-copy:hover .eg-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-button-copy:hover .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy:hover .eg-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy:hover .eg-icon' => 'border-color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy:hover .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-button-copy--icon-view-stacked .eg-icon:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-icon:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-button-copy--icon-view-framed .eg-button-copy-wrap .eg-icon:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Text', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-button-copy-wrap' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_contract',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-button-copy-wrap',
			]
		);


		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_layout_section_controls();
		$this->register_design_icon_section_controls();
		$this->register_design_content_section_controls();
	}

	public function render_element_header() {

		$this->add_render_attribute( 'wrapper', 'class', 'eg-button-copy-box' );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {
		?>
			</div>
		<?php
	}

	public function render_icon() {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
			// add old default
			$settings['icon'] = 'fa fa-clone';
		}

		if ( ! empty( $settings['icon'] ) ) {
			$this->add_render_attribute( 'icon', 'class', $settings['icon'] );
			$this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
		}

		$migrated = isset( $settings['__fa4_migrated']['select_icon'] );
		$is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

		if ( $is_new || $migrated ) {
			Icons_Manager::render_icon( $settings['select_icon'], [ 'aria-hidden' => 'true' ] );
		} else {
			?>
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			<?php
		}
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		$text_default = $settings['text_copy'];

		if ($settings['text_view'] != 'full') {

			$excerpt_left = !empty($settings['text_excerpt_left'])? $settings['text_excerpt_left'] : 0 ;
			$excerpt_right = !empty($settings['text_excerpt_right'])? $settings['text_excerpt_right'] : strlen($text_default) ;

			$text_short = substr($text_default, 0, $excerpt_left).'...'.substr($text_default, (strlen($text_default) == $excerpt_right && empty($settings['text_excerpt_right']) )? $excerpt_right : -$excerpt_right );
		}

		?>

		<div class="eg-button-copy-wrap">
        <span class="eg-button-text">
          <?php echo !empty($text_short)? $text_short :  $text_default; ?>
        </span>
				<span class="eg-button-copy tooltip" data-copy="<?php echo $text_default; ?>">
					<span class="tooltiptext"><?php _e('Copy', 'elementor-gemcrypto'); ?></span>
					<span class="eg-icon">
            <?php echo $this->render_icon(); ?>
          </span>
				</span>
		</div>

		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
