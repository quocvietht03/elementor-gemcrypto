<?php
namespace ElementorGemcrypto\Widgets\Reviews\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Cards extends Skin_Base {

  protected function _register_controls_actions() {
    add_action( 'elementor/element/eg-reviews/section_design_layout/before_section_end', [ $this, 'register_design_layout_section_controls' ] );
    add_action( 'elementor/element/eg-reviews/section_design_box/before_section_end', [ $this, 'register_design_box_section_controls' ] );
    add_action( 'elementor/element/eg-reviews/section_design_image/before_section_end', [ $this, 'register_design_image_section_controls' ] );
    add_action( 'elementor/element/eg-reviews/section_design_content/before_section_end', [ $this, 'register_design_content_section_controls' ] );

	}

	public function get_id() {
		return 'skin-cards';
	}

	public function get_title() {
		return __( 'Cards', 'elementor-gemcrypto' );
	}

  public function register_design_layout_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->parent->update_control(
      'alignment',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'row_gap',
      ]
    );
    $this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
        'default' => 'left',
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'prefix_class' => 'eg-reviews--alignment-',
			]
		);
    $this->parent->end_injection();

  }

  public function register_design_box_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->parent->update_control(
      'box_border_radius',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'box_border_width',
      ]
    );
    $this->add_control(
      'box_border_radius',
      [
        'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
      ]
    );
    $this->parent->end_injection();

    $this->parent->update_control(
      'box_padding',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'box_border_radius',
      ]
    );
    $this->add_control(
      'box_padding',
      [
        'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
      ]
    );
    $this->parent->end_injection();

    $this->parent->update_control(
      'content_padding',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'box_padding',
      ]
    );
    $this->add_control(
      'content_padding',
      [
        'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
      ]
    );
    $this->parent->end_injection();

  }

  public function register_design_image_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->parent->update_control(
      'img_border_radius',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'section_design_image',
      ]
    );
    $this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review-thumbnail' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
    $this->parent->end_injection();

  }

  public function register_design_content_section_controls( Widget_Base $widget )
  {
    $this->parent = $widget;

    $this->parent->update_control(
      'meta_border_radius',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'heading_meta_style',
      ]
    );
    $this->add_control(
      'meta_border_radius',
      [
        'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => [ 'px', '%' ],
        'selectors' => [
          '{{WRAPPER}} .eg-review-item .eg-review__meta' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );
    $this->parent->end_injection();

    $this->parent->update_control(
      'meta_padding',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'meta_border_radius',
      ]
    );
    $this->add_responsive_control(
			'meta_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
    $this->parent->end_injection();

    $this->parent->update_control(
      'meta_background_color',
      [
        'condition' => [
          '_skin' => '',
        ],
      ]
    );
    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'meta_color',
      ]
    );
    $this->add_control(
			'meta_background_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
        'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'background-color: {{VALUE}}',
				],
			]
		);
    $this->parent->end_injection();

    $this->parent->start_injection(
      [
        'at' => 'after',
        'of' => 'heading_custom_link_style',
      ]
    );

    $this->add_control(
      'extra_link_icon_size',
      [
        'label' => __( 'Icon Size', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 50,
          ],
        ],
        'condition' => [
          '_skin' => '',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-reviews--skin-cards .eg-review-item .eg-review__read-more .eg-icon' => 'font-size: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}} .eg-reviews--skin-cards .eg-review-item .eg-review__read-more i' => 'font-size: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}} .eg-reviews--skin-cards .eg-review-item .eg-review__read-more svg' => 'width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      'extra_link_icon_background',
      [
        'label' => __( 'Icon Background', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::COLOR,
        'default' => '',
        'condition' => [
          '_skin' => '',
        ],
        'selectors' => [
          '{{WRAPPER}} .eg-reviews--skin-cards .eg-review-item .eg-review__read-more .eg-icon' => 'background-color: {{VALUE}};',
        ],
      ]
    );

    $this->parent->end_injection();




  }

	public function render() {

		$this->parent->render_element_header();

    ?>
    <div class="elementor-grid eg-list-review">
      <?php
        if( !empty( $this->parent->get_settings('list') ) ) {
          foreach ( $this->parent->get_settings('list') as $key => $value ) {
            $this->parent->render_element_item( $value );
          }
        }
      ?>
    </div>
    <?php

    $this->parent->render_element_show_more();

		$this->parent->render_element_footer();
	}

	protected function content_template() {

	}
}
