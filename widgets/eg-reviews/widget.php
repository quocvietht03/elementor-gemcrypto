<?php
namespace ElementorGemcrypto\Widgets\Reviews;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Css_Filter;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Reviews extends Widget_Base {

	public function get_name() {
		return 'eg-reviews';
	}

	public function get_title() {
		return __( 'EG Reviews', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'font-awesome', 'eg-show-more' ,'eg-reviews' ];
	}

	public function get_script_depends() {
		return ['eg-show-more'];
	}

	protected function _register_skins() {
		$this->add_skin( new Skins\Skin_Cards( $this ) );

	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

    $this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '4',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'prefix_class' => 'elementor-grid%s-',
			]
		);

    $repeater = new Repeater();

    $repeater->add_control(
			'list_image', [
				'label' => __( 'Thumbnail', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$repeater->add_control(
		  'list_meta_text',
		  [
		    'label'   => 'Meta Text',
		    'type'    => Controls_Manager::TEXT,
		    'label_block' => true,
		    'default' => __('This is meta text', 'elementor-gemcrypto'),
		  ]
		);

		$repeater->add_control(
		  'list_meta_link',
		  [
		    'label'   => 'Meta Link',
				'type' => Controls_Manager::URL,
        'label_block' => true,
        'placeholder' => __( 'https://your-link.com', 'elementor-gemcrypto' ),
        'show_external' => true,
        'default' => [
          'url' => '',
          'is_external' => false,
          'nofollow' => false,
        ],
		  ]
		);

		$repeater->add_control(
			'list_meta_icon',
			[
				'label' => esc_html__( 'Meta Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
				'skin' => 'inline',
				'label_block' => false,
			]
		);

		$repeater->add_control(
			'list_meta_icon_align',
			[
				'label' => esc_html__( 'Meta Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
					'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'list_meta_icon[value]!' => '',
				],
			]
		);

    $repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
        'label_block' => true,
				'default' => __( 'This is the title #00', 'elementor-gemcrypto' ),
			]
		);

    $repeater->add_control(
      'list_rating_number',
      [
        'label' => esc_html__( 'Rating Number', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::NUMBER,
        'min' => 0,
        'max' => 5,
        'step' => 0.1,
        'default' => 5,
      ]
    );

    $repeater->add_control(
			'list_rating_tile', [
				'label' => __( 'Rating Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( '100 votes', 'elementor-gemcrypto' ),
			]
		);

    $repeater->add_control(
      'link_text',
      [
        'label' => __( 'Link Text', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXT,
        'label_block' => true,
        'default' => __( 'Read More', 'elementor-gemcrypto' ),
      ]
    );

    $repeater->add_control(
      'link_url',
      [
        'label' => __( 'Link Url', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::URL,
        'label_block' => true,
        'placeholder' => __( 'https://your-link.com', 'elementor-gemcrypto' ),
        'show_external' => true,
        'default' => [
          'url' => '#',
          'is_external' => false,
          'nofollow' => false,
        ],
      ]
    );

    $repeater->add_control(
      'link_selected_icon',
      [
        'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::ICONS,
        'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-long-arrow-alt-right',
					'library' => 'solid',
				],
        'skin' => 'inline',
        'label_block' => false,
      ]
    );

    $repeater->add_control(
      'link_icon_align',
      [
        'label' => esc_html__( 'Icon Position', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SELECT,
        'default' => 'right',
        'options' => [
          'left' => esc_html__( 'Before', 'elementor-gemcrypto' ),
          'right' => esc_html__( 'After', 'elementor-gemcrypto' ),
        ],
        'condition' => [
          'link_selected_icon[value]!' => '',
        ],
      ]
    );

    $repeater->add_control(
      'link_icon_indent',
      [
        'label' => esc_html__( 'Icon Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'max' => 100,
          ],
        ],
				'condition' => [
					'link_selected_icon[value]!' => '',
				],
        'selectors' => [
          '{{WRAPPER}} {{CURRENT_ITEM}} .eg-read-more--icon-after .eg-icon' => 'margin-left: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}} {{CURRENT_ITEM}} .eg-read-more--icon-before .eg-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

		$this->add_control(
			'list',
			[
				'label' => __( 'List', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
            'list_image' => Utils::get_placeholder_image_src(),
						'list_title' => __( 'This is the title #01', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_title' => __( 'This is the title #02', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_title' => __( 'This is the title #03', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_title' => __( 'This is the title #04', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
					[
            'list_image' => Utils::get_placeholder_image_src(),
            'list_title' => __( 'This is the title #05', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
          [
            'list_image' => Utils::get_placeholder_image_src(),
            'list_title' => __( 'This is the title #06', 'elementor-gemcrypto' ),
            'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_title' => __( 'This is the title #07', 'elementor-gemcrypto' ),
						'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
					[
						'list_image' => Utils::get_placeholder_image_src(),
						'list_title' => __( 'This is the title #08', 'elementor-gemcrypto' ),
						'list_rating_number' => 5,
						'list_rating_tile' => __( '100 votes', 'elementor-gemcrypto' ),
						'link_text' => __( 'Read More', 'elementor-gemcrypto' ),
						'link_url' => __( '', 'elementor-gemcrypto' ),
					],
				],
        'title_field' => '{{{ list_title }}}',
			]
		);

		$this->end_controls_section();
	}

	protected function register_rating_stars_section_controls()
	{
		$this->start_controls_section(
			'section_rating_stars',
			[
				'label' => __( 'Rating Stars', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'star_style',
			[
				'label' => esc_html__( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'star_fontawesome' => 'Font Awesome',
					'star_unicode' => 'Unicode',
				],
				'default' => 'star_fontawesome',
				'render_type' => 'template',
				'prefix_class' => 'elementor--star-style-',
			]
		);

		$this->add_control(
			'unmarked_star_style',
			[
				'label' => esc_html__( 'Unmarked Style', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'solid' => [
						'title' => esc_html__( 'Solid', 'elementor-gemcrypto' ),
						'icon' => 'eicon-star',
					],
					'outline' => [
						'title' => esc_html__( 'Outline', 'elementor-gemcrypto' ),
						'icon' => 'eicon-star-o',
					],
				],
				'default' => 'solid',
			]
		);

		$this->add_control(
			'icon_star_space',
			[
				'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-star-rating i:not(:last-of-type)' => 'margin-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_show_more_section_controls() {
		$this->start_controls_section(
			'section_show_more',
			[
				'label' => __( 'Show More', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'show_more',
			[
				'label' => __( 'Show More', 'bearsthemes-addons' ),
				'description' => __( 'Number items in list is greater than init show number.', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'init_show', [
				'label' => __( 'Init Show', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'init_load', [
				'label' => __( 'Init Load', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_more_text',
			[
				'label' => __( 'Show More Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Show More', 'elementor-gemcrypto' ),
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_less_text',
			[
				'label' => __( 'Show Less Text', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Show Less', 'elementor-gemcrypto' ),
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_latyout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'column_gap',
			[
				'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'row_gap',
			[
				'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'prefix_class' => 'eg-reviews--alignment-',
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_box_section_controls() {
		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'bg_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-review-item',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-review-item:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

  protected function register_design_image_section_controls() {
		$this->start_controls_section(
			'section_design_image',
			[
				'label' => __( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review-thumbnail' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'thumbnail_effects_tabs' );

		$this->start_controls_tab( 'normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_filters',
				'selector' => '{{WRAPPER}} .eg-review-item .eg-review-thumbnail img',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_hover_filters',
				'selector' => '{{WRAPPER}} .eg-review-item:hover .eg-review-thumbnail img',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


  public function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_meta_style',
			[
				'label' => __( 'Meta', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-review-item .eg-review__meta',
			]
		);

		$this->add_control(
			'meta_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'meta_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs('tabs_meta');

		$this->start_controls_tab(
			'tabs_meta_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			],
		);

		$this->add_control(
			'meta_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_background_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'meta_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'meta_border_width' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tabs_meta_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			],
		);

		$this->add_control(
			'meta_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_background_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'meta_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'meta_border_width' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__meta:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();


		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__title' => 'color: {{VALUE}};',
				],
			]
		);

    $this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-review-item .eg-review__title',
			]
		);

		$this->add_control(
			'heading_start_rating_style',
			[
				'label' => __( 'Start Rating', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'start_rating_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__rating  i' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-review-item .eg-review__rating svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'start_rating_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__rating' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'start_rating_color_active',
			[
				'label' => __( 'Color Active', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__rating i:before' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'heading_start_rating_title_style',
			[
				'label' => __( 'Rating Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'rating_title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__rating-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'rating_title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-review-item .eg-review__rating-title',
			]
		);

		$this->add_control(
			'heading_custom_link_style',
			[
				'label' => __( 'Custom Link', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'extra_link_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-read-more--extra-link-view-',
			]
		);

		$this->add_control(
			'extra_link_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-stacked .eg-review__read-more,
					 {{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'extra_link_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'extra_link_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-stacked .eg-review__read-more,
					 {{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'extra_link_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-reviews .eg-review-item .eg-review__read-more',
			]
		);

		$this->start_controls_tabs( 'tabs_extra_link' );

		$this->start_controls_tab(
			'tab_extra_link_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'extra_link_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__read-more' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extra_link_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-stacked .eg-review__read-more' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extra_link_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-icon-box .eg-link-btn' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_extra_link_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'extra_link_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-review-item .eg-review__read-more:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extra_link_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-stacked .eg-review__read-more:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'extra_link_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'extra_link_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-read-more--extra-link-view-framed .eg-review__read-more:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();


		$this->end_controls_section();
	}


	public function register_design_show_more_section_controls() {
		$this->start_controls_section(
			'section_design_show_more',
			[
				'label' => __( 'Show More', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_more!' => '',
				],
			]
		);

		$this->add_control(
			'show_more_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-show-more--btn-view-',
			]
		);

		$this->add_control(
			'show_more_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'show_more_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'show_more_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a,
					 {{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'show_more_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-show-more a',
			]
		);

		$this->start_controls_tabs( 'tabs_show_more' );

		$this->start_controls_tab(
			'tab_show_more_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_show_more_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'show_more_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-show-more a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'show_more_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-stacked .eg-show-more a:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'show_more_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'show_more_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-show-more--btn-view-framed .eg-show-more a:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();
		$this->register_rating_stars_section_controls();
		$this->register_show_more_section_controls();

		$this->register_design_latyout_section_controls();
    $this->register_design_box_section_controls();
    $this->register_design_image_section_controls();
    $this->register_design_content_section_controls();
		$this->register_design_show_more_section_controls();
	}

	public function get_instance_value_skin( $key ) {
		$settings = $this->get_settings_for_display();

		if( !empty( $settings['_skin'] ) && isset( $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key] ) ) {
			 return $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key];
		}
		return $settings[$key];
	}

	public function render_element_header() {
		$settings = $this->get_settings_for_display();

		$classes = 'eg-reviews';

		if (!empty( $settings['_skin']) ) {
			$classes .= ' '.$settings['_skin'];
		}

		if( $settings['show_more'] === 'yes' ) {
			$classes .= ' has-show-more';
		}

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		?>
			<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {

		?>
			</div>
		<?php
	}

	public function render_element_show_more() {
		$settings = $this->get_settings_for_display();
		if( $settings['show_more'] !== 'yes' || count( $settings['list'] ) <= $settings['init_show'] ) {
			return;
		}

		echo '<div class="eg-show-more" data-show="' . $settings['init_show'] . '" data-load="' . $settings['init_load'] . '" >
						<a class="eg-show-more-btn" href="#">' . $settings['show_more_text'] . '</a>
						<a class="eg-show-less-btn" href="#">' . $settings['show_less_text'] . '</a>
					</div>';
	}

  public function render_link_icon( $value, $icon ) {

    if ( empty( $value['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
      // add old default
      $value['icon'] = 'fa fa-star';
    }

    if ( ! empty( $value['icon'] ) ) {
      $this->add_render_attribute( 'icon', 'class', $value['icon'] );
      $this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
    }

    $migrated = isset( $value['__fa4_migrated'][$icon] );
    $is_new = empty( $value['icon'] ) && Icons_Manager::is_migration_allowed();

    if ( $is_new || $migrated ) {
			echo '<span class="eg-icon"> ';
      	Icons_Manager::render_icon( $value[$icon], [ 'aria-hidden' => 'true' ] );
			echo '</span>';
		} else {
      ?>
			<span class="eg-icon">
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			</span>
      <?php
    }
  }

  public function render_stars( $value ) {
		$settings = $this->get_settings_for_display();

    $rating_data = $value['list_rating_number'];
    $rating = (float) $rating_data;
    $floored_rating = floor( $rating );
    $stars_html = '';
    $icon = '&#xE934;';

		if ( 'star_fontawesome' === $settings['star_style'] ) {
			if ( 'outline' === $settings['unmarked_star_style'] ) {
				$icon = '&#xE933;';
			}
		} elseif ( 'star_unicode' === $settings['star_style'] ) {
			$icon = '&#9733;';

			if ( 'outline' === $settings['unmarked_star_style'] ) {
				$icon = '&#9734;';
			}
		}

    for ( $stars = 1.0; $stars <= 5; $stars++ ) {
      if ( $stars <= $floored_rating ) {
        $stars_html .= '<i class="elementor-star-full">' . $icon . '</i>';
      } elseif ( $floored_rating + 1 === $stars && $rating !== $floored_rating ) {
        $stars_html .= '<i class="elementor-star-' . ( $rating - $floored_rating ) * 10 . '">' . $icon . '</i>';
      } else {
        $stars_html .= '<i class="elementor-star-empty">' . $icon . '</i>';
      }
    }

    $stars_element = '<span class="elementor-star-rating eg-review__rating">' . $stars_html . '</span>';

    return $stars_element;
  }

	public function render_element_item( $value ) {

		$attachment = wp_get_attachment_image_src( $value['list_image']['id'], 'full' );
		$thumbnail = !empty( $attachment ) ? $attachment[0] : $value['list_image']['url'];


    if( $value['link_url'] != '' ) {
      $this->remove_render_attribute( 'custom-link-url' );
      $this->add_link_attributes( 'custom-link-url', $value['link_url'] );
    }

		if ( $value['list_meta_link'] != '' ) {
			$this->remove_render_attribute('meta-link');
			$this->add_link_attributes('meta-link', $value['list_meta_link'] );
		}

		echo '<div class="elementor-item eg-review-item elementor-repeater-item-'. $value['_id'] .'">';

			echo '<div class="eg-review-thumbnail">';
				if( !empty( $value['link_url'] ) ) {
					echo '<a ' .  $this->get_render_attribute_string( 'custom-link-url' ) . '><img src=" ' . esc_url( $thumbnail ) . ' " alt=""></a>';
				} else {
					echo '<img src=" ' . esc_url( $thumbnail ) . ' " alt="">';
				}
			echo '</div>';

			echo '<div class="eg-review-content">';
				$meta_icon_align_class = ($value['list_meta_icon_align'] == 'right')? 'eg-review__meta--icon-after' : 'eg-review__meta--icon-before';
				if ( !empty( $value['list_meta_text'] ) ) {
					if ($value['list_meta_link']) {
						echo '<div class="eg-review__meta ' . $meta_icon_align_class . '"><a ' . $this->get_render_attribute_string( 'meta-link' ) . '>';
							if ( $value['list_meta_icon_align'] == 'right' ) {
								echo '<span class="eg-review__meta-text">' . $value['list_meta_text']  . '</span>';
								$this->render_link_icon($value, 'list_meta_icon');
							}else {
								$this->render_link_icon($value, 'list_meta_icon');
								echo '<span class="eg-review__meta-text">' . $value['list_meta_text']  . '</span>';
							}
						echo '</a></div>';
					}else {
						echo '<div class="eg-review__meta ' . $meta_icon_align_class . '">';
							if ( $value['list_meta_icon_align'] == 'right') {
								echo '<span class="eg-review__meta-text">' . $value['list_meta_text']  . '</span>';
								$this->render_link_icon($value, 'list_meta_icon');
							}else {
								$this->render_link_icon($value, 'list_meta_icon');
								echo '<span class="eg-review__meta-text">' . $value['list_meta_text']  . '</span>';
							}
						echo '</div>';
					}
				}
				if( !empty( $value['list_title'] ) ) {
					if( $value['link_url'] != '' ) {
						echo '<h3 class="eg-review__title"><a '. $this->get_render_attribute_string( 'custom-link-url' ) .'>' . $value['list_title'] . '</a></h3>';
					} else {
						echo '<h3 class="eg-review__title">' . $value['list_title'] . '</h3>';
					}
				}

        echo '<div class="eg-review-rating-wrapper">';
				    echo $this->render_stars($value);
            if ( !empty( $value['list_rating_tile']) ) {
              echo '<span class="eg-review__rating-title">' . $value['list_rating_tile'] . '</span>' ;
            };
        echo '</div>';

        if( $value['link_url'] != '' && $value['link_text'] != '' ) {
          if( !empty( $value['link_selected_icon']['value'] ) ) {
            if( $value['link_icon_align']  == 'right' ) {
              echo '<a class="eg-review__read-more eg-read-more--icon-after" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>';
							  echo '<span>' . $value['link_text'] . '</span>';
                $this->render_link_icon($value, 'link_selected_icon');
              echo '</a>';
            } else {
              echo '<a class="eg-review__read-more eg-read-more--icon-before" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>';
                $this->render_link_icon($value, 'link_selected_icon');
              	echo '<span>' . $value['link_text'] . '</span>';
							echo '</a>';
            }
          } else {
            echo '<a class="eg-review__read-more" ' . $this->get_render_attribute_string( 'custom-link-url' ) . '>' . $value['link_text'] . '</a>';
          }
        }

			echo '</div>';

		echo '</div>';
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();

		?>

		<div class="elementor-grid eg-list-review">
			<?php
        if( !empty( $settings['list'] ) ) {
          foreach ( $settings['list'] as $key => $value ) {
            $this->render_element_item( $value );
          }
        }
      ?>
		</div>

		<?php

		$this->render_element_show_more();

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
