<?php
namespace ElementorGemcrypto\Widgets\Projects_Carousel;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Projects_Carousel extends Widget_Base {

	public function get_name() {
		return 'eg-projects-carousel';
	}

	public function get_title() {
		return __( 'EG Projects Carousel', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-widget-carousel', 'eg-projects' ];
	}

	public function get_script_depends() {
		return ['eg-swiper', 'eg-widget-carousel', 'eg-projects'];
	}

	protected function _register_skins() {
		$this->add_skin( new Skins\Skin_Sidu( $this ) );

	}

	protected function get_supported_ids() {
		$supported_ids = [];

		$wp_query = new \WP_Query( array(
										'post_type' => 'projects',
										'post_status' => 'publish'
									) );

		if ( $wp_query->have_posts() ) {
	    while ( $wp_query->have_posts() ) {
        $wp_query->the_post();
        $supported_ids[get_the_ID()] = get_the_title();
	    }
		}

		return $supported_ids;
	}

	public function get_supported_taxonomies() {
		$supported_taxonomies = [];

		$categories = get_terms( array(
			'taxonomy' => 'projects_category',
	    'hide_empty' => false,
		) );
		if( ! empty( $categories )  && ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
			    $supported_taxonomies[$category->term_id] = $category->name;
			}
		}

		return $supported_taxonomies;
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

    $this->add_responsive_control(
			'sliders_per_view',
			[
				'label' => __( 'Slides Per View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->add_control(
			'sliders_per_column',
			[
				'label' => __( 'Slides Per Column', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				],
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail',
				'default' => 'medium',
				'exclude' => [ 'custom' ],
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->add_responsive_control(
			'image_ratio',
			[
				'label' => __( 'Image Ratio', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0.66,
				],
				'range' => [
					'px' => [
						'min' => 0.3,
						'max' => 2,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project__featured' => 'padding-bottom: calc( {{SIZE}} * 100% );',
				],
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_query_section_controls() {
		$this->start_controls_section(
			'section_query',
			[
				'label' => __( 'Query', 'elementor-gemcrypto' ),
			]
		);

		$this->start_controls_tabs( 'tabs_query' );

		$this->start_controls_tab(
			'tab_query_include',
			[
				'label' => __( 'Include', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'ids',
			[
				'label' => __( 'Ids', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $this->get_supported_ids(),
				'label_block' => true,
				'multiple' => true,
			]
		);

		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $this->get_supported_taxonomies(),
				'label_block' => true,
				'multiple' => true,
			]
		);

		$this->end_controls_tab();


		$this->start_controls_tab(
			'tab_query_exnlude',
			[
				'label' => __( 'Exclude', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'ids_exclude',
			[
				'label' => __( 'Ids', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $this->get_supported_ids(),
				'label_block' => true,
				'multiple' => true,
			]
		);

		$this->add_control(
			'category_exclude',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $this->get_supported_taxonomies(),
				'label_block' => true,
				'multiple' => true,
			]
		);

		$this->add_control(
			'offset',
			[
				'label' => __( 'Offset', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 0,
				'description' => __( 'Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'elementor-gemcrypto' ),
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'orderby',
			[
				'label' => __( 'Order By', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date' => __( 'Date', 'elementor-gemcrypto' ),
					'post_title' => __( 'Title', 'elementor-gemcrypto' ),
					'menu_order' => __( 'Menu Order', 'elementor-gemcrypto' ),
					'rand' => __( 'Random', 'elementor-gemcrypto' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __( 'ASC', 'elementor-gemcrypto' ),
					'desc' => __( 'DESC', 'elementor-gemcrypto' ),
				],
			]
		);

		$this->end_controls_section();
	}

  protected function register_additional_section_controls() {
		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'bearsthemes-addons' ),
			]
		);

    $this->add_control(
			'navigation',
			[
				'label' => __( 'Prev & Next Button', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'nav_btn_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'navigation!' => '',
				],
				'prefix_class' => 'eg-swiper-button--view-',
			]
		);

    $this->add_control(
			'pagination',
			[
				'label' => __( 'Dots', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Transition Duration', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'bearsthemes-addons' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'loop',
			[
				'label' => __( 'Infinite Loop', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->end_controls_section();
	}

  protected function register_design_latyout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_responsive_control(
			'space_between',
			[
				'label' => __( 'Space Between', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'size' => 30,
				],
			]
		);

		$this->add_responsive_control(
			'items_space_between',
			[
				'label' => __( 'Items Space Between', 'bearsthemes-addons' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'size' => 30,
				],
        'selectors' => [
					'{{WRAPPER}} .elementor-item:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_box_section_controls() {
		$this->start_controls_section(
			'section_design_box',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'box_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'border-radius: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => __( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-project__content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'bg_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-project',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-project' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-project:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-project:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-project:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_image_section_controls() {
		$this->start_controls_section(
			'section_design_image',
			[
				'label' => __( 'Image', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-project__featured' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs(
			'thumbnail_effects_tabs',
		);

		$this->start_controls_tab( 'normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_filters',
				'selector' => '{{WRAPPER}} .eg-project__featured img',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_hover_filters',
				'selector' => '{{WRAPPER}} .eg-project:hover .eg-project__featured img',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'_skin' => '',
				],
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-project__title' => 'color: {{VALUE}};',
				],
			]
		);

    $this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-project__title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-project__title',
			]
		);

		$this->add_control(
			'heading_category_style',
			[
				'label' => __( 'Category', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'category_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-project__category' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'category_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-project__category a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'category_typography',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-project__category',
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_navigation_section_controls() {
		$this->start_controls_section(
			'section_design_navigation',
			[
				'label' => __( 'Prev & Next Button', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation!' => '',
				],
			]
		);

		$this->add_control(
			'navigation_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'navigation_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'condition' => [
					'nav_btn_view!' => '',
				],
			]
		);

		$this->add_control(
			'navigation_border',
			[
				'label' => __( 'Border Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'navigation_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button,
					 {{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'navigation_style' );

		$this->start_controls_tab(
			'navigation_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'navigation_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'navigation_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'navigation_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-stacked .eg-swiper-button:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'navigation_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'nav_btn_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-swiper-button--view-framed .eg-swiper-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_pagination_section_controls() {
		$this->start_controls_section(
			'section_design_pagination',
			[
				'label' => __( 'Dots', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'pagination!' => '',
				],
			]
		);

		$this->add_control(
			'pagination_size',
			[
				'label' => __( 'Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'pagination_space_between',
			[
				'label' => esc_html__( 'Space Between', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet' => 'margin-left: {{SIZE}}{{UNIT}}; margin-right: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
			]
		);

		$this->start_controls_tabs( 'pagination_style' );

		$this->start_controls_tab(
			'pagination_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet:not(.swiper-pagination-bullet-active)' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'pagination_active',
			[
				'label' => __( 'Active', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'pagination_color_active',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-swiper-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();
		$this->register_query_section_controls();
    $this->register_additional_section_controls();

		$this->register_design_latyout_section_controls();
    $this->register_design_box_section_controls();
    $this->register_design_image_section_controls();
    $this->register_design_content_section_controls();
		$this->register_design_navigation_section_controls();
		$this->register_design_pagination_section_controls();
	}

	public function get_instance_value_skin( $key ) {
		$settings = $this->get_settings_for_display();

		if( !empty( $settings['_skin'] ) && isset( $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key] ) ) {
			return $settings[str_replace( '-', '_', $settings['_skin'] ) . '_' . $key];
		}

		if( isset( $settings[$key] ) ) {
			return $settings[$key];
		}

		return;
	}

	public function query_posts() {
		$settings = $this->get_settings_for_display();

		if( is_front_page() ) {
	    $paged = (get_query_var('page')) ? absint( get_query_var('page') ) : 1;
		} else {
	    $paged = (get_query_var('paged')) ? absint( get_query_var('paged') ) : 1;
		}

		$args = [
			'post_type' => 'projects',
			'post_status' => 'publish',
			'posts_per_page' => $this->get_instance_value_skin('posts_per_page'),
			'paged' => $paged,
			'orderby' => $settings['orderby'],
			'order' => $settings['order'],
		];

		if( ! empty( $settings['ids'] ) ) {
			$args['post__in'] = $settings['ids'];
		}

		if( ! empty( $settings['ids_exclude'] ) ) {
			$args['post__not_in'] = $settings['ids_exclude'];
		}

		if( ! empty( $settings['category'] ) ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' 		=> 'projects_category',
					'terms' 		=> $settings['category'],
					'field' 		=> 'term_id',
					'operator' 		=> 'IN'
				)
			);
		}

		if( ! empty( $settings['category_exclude'] ) ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' 		=> 'projects_category',
					'terms' 		=> $settings['category_exclude'],
					'field' 		=> 'term_id',
					'operator' 		=> 'NOT IN'
				)
			);
		}

		if( 0 !== absint( $settings['offset'] ) ) {
			$args['offset'] = $settings['offset'];
		}

		return $query = new \WP_Query( $args );
	}

	protected function swiper_data() {
		$settings = $this->get_settings_for_display();

		$slides_per_view = $this->get_instance_value_skin('sliders_per_view');
		$slides_per_view_tablet = $this->get_instance_value_skin('sliders_per_view_tablet') ? $this->get_instance_value_skin('sliders_per_view_tablet') : 2;
		$slides_per_view_mobile = $this->get_instance_value_skin('sliders_per_view_mobile') ? $this->get_instance_value_skin('sliders_per_view_mobile') : 1;

		$space_between = ( '' !== $settings['space_between']['size'] ) ? $settings['space_between']['size'] : 30;
		$space_between_tablet = ( isset( $settings['space_between_tablet']['size'] ) && '' !== $settings['space_between_tablet']['size'] ) ? $settings['space_between_tablet']['size'] : $space_between;
		$space_between_mobile = ( isset( $settings['space_between_mobile']['size'] ) && '' !== $settings['space_between_mobile']['size'] ) ? $settings['space_between_mobile']['size'] : $space_between_tablet;

		$swiper_data = array(
			'slidesPerView' => $slides_per_view_mobile,
			'spaceBetween' => $space_between_mobile,
			'speed' => $settings['speed'],
			'loop' => $settings['loop'] == 'yes' ? true : false,
			'breakpoints' => array(
				767 => array(
				  'slidesPerView' => $slides_per_view_tablet,
				  'spaceBetween' => $space_between_tablet,
				),
				1025 => array(
				  'slidesPerView' => $slides_per_view,
				  'spaceBetween' => $space_between,
				)
			),

		);

		if( $settings['navigation'] === 'yes' ) {
			$swiper_data['navigation'] = array(
				'nextEl' => '.eg-swiper-button-next__' . $this->get_id(),
				'prevEl' => '.eg-swiper-button-prev__' . $this->get_id(),
			);
		}

		if( $settings['pagination'] === 'yes' ) {

			$swiper_data['pagination'] = array(
				'el' => '.eg-swiper-pagination__' . $this->get_id(),
				'type' => 'bullets',
				'clickable' => true,
			);
		}

		if( $settings['autoplay'] === 'yes' ) {
			$swiper_data['autoplay'] = array(
				'delay' => $settings['autoplay_speed'],
			);
		}

		return $swiper_json = json_encode($swiper_data);
	}

	public function render_element_header() {
    $settings = $this->get_settings_for_display();

    $classes = 'swiper-container eg-projects';

		if( !empty( $settings['_skin'] ) ) {
			$classes .= ' eg-projects--' . $settings['_skin'];
		} else {
			$classes .= ' eg-projects--skin-default';
		}

    if( $settings['navigation'] === 'yes' ) {
      $classes .= ' has-navigation';
    }

    if( $settings['pagination'] === 'yes' ) {
      $classes .= ' has-pagination';
    }

		$this->add_render_attribute( 'wrapper', 'class', $classes );

    $this->add_render_attribute( 'wrapper', 'data-swiper', $this->swiper_data() );

    echo '<div ' . $this->get_render_attribute_string( 'wrapper' ) . '>';
	}

	public function render_loop_header() {
		$settings = $this->get_settings_for_display();

		$classes = 'swiper-wrapper eg-list-posts';

		$this->add_render_attribute( 'loop', 'class', $classes );
		?>
			<div <?php echo $this->get_render_attribute_string( 'loop' ); ?>>
		<?php
	}

	public function render_loop_footer() {

		?>
			</div>
		<?php
	}

	public function render_element_footer() {
    $settings = $this->get_settings_for_display();

		echo '</div>';

    if( $settings['navigation'] === 'yes' ) {
      echo '<div class="eg-swiper-button eg-swiper-button-prev eg-swiper-button-prev__' . $this->get_id() . '"><i class="fa fa-chevron-left"></i></div>
            <div class="eg-swiper-button eg-swiper-button-next eg-swiper-button-next__' . $this->get_id() . '"><i class="fa fa-chevron-right"></i></div>';
    }

    if( $settings['pagination'] === 'yes' ) {
      echo '<div class="eg-swiper-pagination eg-swiper-pagination__' . $this->get_id() . '"></div>';
    }
	}

	public function render_post() {
		$settings = $this->get_settings_for_display();

		?>
			<article id="post-<?php the_ID();  ?>" <?php post_class( 'elementor-item eg-project' ); ?>>
				<div class="eg-project__thumbnail">
					<a href="<?php the_permalink() ?>">
						<div class="eg-project__featured"><?php the_post_thumbnail( $settings['thumbnail_size'] ); ?></div>
					</a>
				</div>

				<div class="eg-project__content">
					<?php
						the_title( '<h3 class="eg-project__title"><a href="' . get_the_permalink() . '">', '</a></h3>' );

						the_terms( get_the_ID(), 'projects_category', '<div class="eg-project__category">', ' | ', '</div>' );

					?>
				</div>
			</article>
		<?php
	}

	protected function render() {
    $settings = $this->get_settings_for_display();

		$query = $this->query_posts();

		$this->render_element_header();

			if ( $query->have_posts() ) {
        $total = $query->post_count;
        $key = 0;
        $count = 0;

				$this->render_loop_header();

					while ( $query->have_posts() ) {
						$query->the_post();

            $count++;

						if( $count == 1 ) {
							echo '<div class="swiper-slide">';
						}

						$this->render_post();

            if( $count == $settings['sliders_per_column'] || $total == $key ) {
	            echo '</div>';
						}

						if( $count == $settings['sliders_per_column'] ) $count = 0;

            $key++;
					}

				$this->render_loop_footer();

			} else {
			    echo '<div class="eg-no-posts-found">' . esc_html__('No posts found!', 'elementor-gemcrypto') . '</div>';
			}
			wp_reset_postdata();

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
