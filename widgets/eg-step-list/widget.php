<?php
namespace ElementorGemcrypto\Widgets\Step_List;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Step_List extends Widget_Base {

	public function get_name() {
		return 'eg-step-list';
	}

	public function get_title() {
		return __( 'EG Step List', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-step-list' ];
	}

	public function get_script_depends() {
		return [];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'prefix_class' => 'elementor-grid%s-',
			]
		);

    $repeater = new Repeater();

    $repeater->add_control(
      'step_title',
      [
        'label' => esc_html__( 'Step Title', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXT,
				'label_block' => true,
        'default' => __('This is the title', 'elementor-gemcrypto' ),
      ]
    );

    $repeater->add_control(
      'step_description',
      [
        'label' => esc_html__( 'Step Description', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::TEXTAREA,
        'default' => __('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor-gemcrypto' ),
      ]
    );


    $this->add_control(
      'step_list',
      [
        'label' => esc_html__( 'Step', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'step_number' => '01',
            'step_title' => 'This is the title 01',
            'step_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
          ],
          [
            'step_number' => '02',
            'step_title' => 'This is the title 02',
            'step_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
          ],
          [
            'step_number' => '03',
            'step_title' => 'This is the title 03',
            'step_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
          ],
        ],
        'title_field' => '{{{ step_title }}}',
      ]
    );

		$this->end_controls_section();

		$this->start_controls_section(
			'section_layout_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'step_number_view',
			[
				'label' => __( 'Number View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-step-list--number-view-',
			]
		);

		$this->add_control(
			'step_number_position',
			[
				'label' => __( 'Number Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => 'center',
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Top', 'elementor-gemcrypto' ),
						'icon' => 'eicon-v-align-top',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'eg-step-list--number-position-',
			]
		);

		$this->end_controls_section();

	}

	protected function register_design_layout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'column_gap',
			[
				'label' => __( 'Columns Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-column-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'row_gap',
			[
				'label' => __( 'Rows Gap', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 30,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '--grid-row-gap: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'alignment',
			[
				'label' => esc_html__( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => '',
				'options' => [
					'left'    => [
						'title' => esc_html__( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'prefix_class' => 'eg-icon-box--horizontal-align-',
				'selectors' => [
					'{{WRAPPER}} .eg-step-list .eg-step-box' => 'text-align: {{VALUE}}',
					'{{WRAPPER}} .eg-step-list .eg-content' => 'flex-grow: 1',
				],
			]
		);

		$this->add_control(
			'vertical_ignment',
			[
				'label' => __( 'Vertical Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'flex-start',
				'options' => [
					'flex-start' => __( 'Top', 'elementor-gemcrypto' ),
					'center' => __( 'Middle', 'elementor-gemcrypto' ),
					'flex-end' => __( 'Bottom', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'step_number_position' => [ 'left', 'right' ],
				],
				'prefix_class' => 'eg-step-number--vertical-align-',
        'selectors' => [
          '{{WRAPPER}} .eg-step-box' => 'align-items: {{VALUE}}',
        ],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_step_box_section_controls() {
		$this->start_controls_section(
			'step_box_section',
			[
				'label' => __( 'Box', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'step_box_border_width',
			[
				'label' => __( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-step-box' => 'border-style: solid; border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'step_box_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-step-box' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'step_box_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .eg-step-box' => 'padding-top: {{TOP}}{{UNIT}}; padding-right: {{RIGHT}}{{UNIT}}; padding-bottom: {{BOTTOM}}{{UNIT}}; padding-left: {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'step_box_effects_tabs' );

		$this->start_controls_tab( 'classic_style_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'selector' => '{{WRAPPER}} .eg-step-box',
			]
		);

		$this->add_control(
			'box_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-step-box' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-step-box' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'classic_style_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}} .eg-step-box:hover',
			]
		);

		$this->add_control(
			'box_bg_color_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-step-box:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'box_border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eg-step-box:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_design_step_number_controls() {
		$this->start_controls_section(
			'step_number_section',
			[
				'label' => __( 'Number', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'step_number_padding',
			[
				'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .eg-step-number' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'step_number_view!' => '',
				],
			]
		);

		$this->add_control(
			'ste_number_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-step-number' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'step_number_view!' => '',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'step_number_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-step-list--number-view-framed .eg-step-number' => 'border-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'step_number_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-step-list--number-view-framed .eg-step-number' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_step_number',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-step-number',
			]
		);

		$this->add_control(
			'step_number_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-step-number' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'step_number_background_color',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'step_number_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-step-list--number-view-stacked .eg-step-number' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-step-list--number-view-framed .eg-step-number' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
      'step_number_space',
      [
        'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
					'{{WRAPPER}} .eg-step-list .eg-step-number-box .eg-step-number' => 'margin-bottom: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}}.eg-step-list--number-position-left .eg-step-number-box .eg-step-number' => 'margin-right: {{SIZE}}{{UNIT}}; margin-bottom: 0;',
          '{{WRAPPER}}.eg-step-list--number-position-center .eg-step-number-box .eg-step-number' => 'margin-bottom: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}}.eg-step-list--number-position-right .eg-step-number-box .eg-step-number' => 'margin-left: {{SIZE}}{{UNIT}}; margin-bottom: 0;',
        ],
      ]
    );

		$this->end_controls_section();
	}

	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Content', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_title_style',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'step_title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-step-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_step_title',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-step-title',
			]
		);

		$this->add_control(
			'heading_description_style',
			[
				'label' => __( 'Description', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'step_description_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-step-des' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_step_description',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-step-des',
			]
		);

		$this->add_control(
			'step_description_space',
			[
				'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-step-des' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_layout_section_controls();
		$this->register_design_step_box_section_controls();
		$this->register_design_step_number_controls();
		$this->register_design_content_section_controls();
	}

	public function render_element_header() {

		$this->add_render_attribute( 'wrapper', 'class', 'elementor-grid eg-step-list' );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {
		?>
			</div>
		<?php
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->render_element_header();


		?>
		<?php if (!empty($settings['step_list'])): ?>
			<?php foreach ($settings['step_list'] as $key => $step): $zero = ( ($key + 1) < 10)? ' 0': ''; ?>
				<div class="eg-step-box">
					<div class="eg-step-number-box">
						<?php if ( !empty($step['step_title']) || !empty($step['step_description'])): ?>
							<span class="eg-step-number"><?php echo $zero.($key + 1) ; ?></span>
						<?php endif; ?>
					</div>
					<div class="eg-step-content">
						<?php if ( !empty($step['step_title']) ): ?>
							<h4 class="eg-step-title"><?php echo  $step['step_title']; ?></h4>
						<?php endif; ?>
						<?php if ( !empty($step['step_description']) ): ?>
							<p class="eg-step-des"><?php echo $step['step_description']; ?></p>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>

		<?php
		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
