<?php
namespace ElementorGemcrypto\Widgets\Icon_Text;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class EG_Icon_Text extends Widget_Base {

	public function get_name() {
		return 'eg-icon-text';
	}

	public function get_title() {
		return __( 'EG Icon Text', 'elementor-gemcrypto' );
	}

	public function get_icon() {
		return 'eg-icon';
	}

	public function get_categories() {
		return [ 'elementor-gemcrypto' ];
	}

	public function get_style_depends() {
		return [ 'eg-icon-text' ];
	}

	public function get_script_depends() {
		return [];
	}

	protected function register_layout_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'select_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fa fa-star',
					'library' => 'fa-solid',
				],
			]
		);

		$this->add_control(
			'icon_view',
			[
				'label' => __( 'View', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'elementor-gemcrypto' ),
					'stacked' => __( 'Stacked', 'elementor-gemcrypto' ),
					'framed' => __( 'Framed', 'elementor-gemcrypto' ),
				],
				'prefix_class' => 'eg-icon-box--icon-view-',
			]
		);

		$this->add_control(
			'icon_shape',
			[
				'label' => __( 'Shape', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'circle',
				'options' => [
					'circle' => __( 'Circle', 'elementor-gemcrypto' ),
					'square' => __( 'Square', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_view!' => '',
				],
				'prefix_class' => 'eg-icon-box--icon-shape-',
			]
		);

		$this->add_control(
			'icon_position',
			[
				'label' => __( 'Icon Position', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => 'center',
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Top', 'elementor-gemcrypto' ),
						'icon' => 'eicon-v-align-top',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'eg-icon-box--icon-position-',
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'This is the heading', 'elementor-gemcrypto' ),
			]
		);

    $this->add_control(
      'title_link',
			[
        'label' => esc_html__( 'Link', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::URL,
				'default' => [],
        'dynamic' => [
          'active' => false,
        ],
        'placeholder' => esc_html__( 'https://your-link.com', 'elementor-gemcrypto' ),
      ]
    );

		$this->end_controls_section();
	}

	protected function register_design_layout_section_controls() {
		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Layout', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'title_alignment',
			[
				'label' => esc_html__( 'Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => '',
				'options' => [
					'left'    => [
						'title' => esc_html__( 'Left', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'elementor-gemcrypto' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'prefix_class' => 'eg-icon-box--horizontal-align-',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box' => 'text-align: {{VALUE}}',
					'{{WRAPPER}} .eg-icon-box .eg-content' => 'flex-grow: 1',
				],
			]
		);

		$this->add_control(
			'vertical_ignment',
			[
				'label' => __( 'Vertical Alignment', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'flex-start',
				'options' => [
					'flex-start' => __( 'Top', 'elementor-gemcrypto' ),
					'center' => __( 'Middle', 'elementor-gemcrypto' ),
					'flex-end' => __( 'Bottom', 'elementor-gemcrypto' ),
				],
				'condition' => [
					'icon_position!' => 'center',
				],
				'prefix_class' => 'eg-icon-box--vertical-align-',
        'selectors' => [
          '{{WRAPPER}} .eg-icon-box' => 'align-items: {{VALUE}}',
        ],
			]
		);

		$this->end_controls_section();
	}

	protected function register_design_icon_section_controls() {
		$this->start_controls_section(
			'section_design_icon',
			[
				'label' => __( 'Icon', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'icon_size',
			[
				'label' => __( 'Icon Size', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 300,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .eg-icon svg' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_space',
			[
				'label' => esc_html__( 'Spacing', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box .eg-icon-wrap' => 'margin-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.eg-icon-box--icon-position-left .eg-icon-box .eg-icon-wrap' => 'margin-right: {{SIZE}}{{UNIT}}; margin-bottom: 0;',
					'{{WRAPPER}}.eg-icon-box--icon-position-center .eg-icon-box .eg-icon-wrap' => 'margin-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.eg-icon-box--icon-position-right .eg-icon-box .eg-icon-wrap' => 'margin-left: {{SIZE}}{{UNIT}}; margin-bottom: 0;',
				],
			]
		);

    $this->add_control(
      'icon_padding',
      [
        'label' => esc_html__( 'Padding', 'elementor-gemcrypto' ),
        'type' => Controls_Manager::SLIDER,
        'selectors' => [
          '{{WRAPPER}} .eg-icon-box .eg-icon' => 'padding: {{SIZE}}{{UNIT}};',
        ],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'condition' => [
          'icon_view!' => '',
        ],
      ]
    );

		$this->add_control(
			'border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box .eg-icon' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'icon_view!' => '',
				],
			]
		);

		$this->add_control(
			'border_width',
			[
				'label' => esc_html__( 'Border Width', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'icon_view' => 'framed',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_icon' );

		$this->start_controls_tab(
			'tab_icon_normal',
			[
				'label' => __( 'Normal', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box .eg-icon' => 'border-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box .eg-icon-wrap .eg-icon' => 'border-color: {{VALUE}};',

				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_icon_hover',
			[
				'label' => __( 'Hover', 'elementor-gemcrypto' ),
			]
		);

		$this->add_control(
			'icon_color_hover',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-icon-box:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .eg-icon-box:hover .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon-box:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon-box:hover .eg-icon svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box:hover .eg-icon i' => 'color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box:hover .eg-icon' => 'border-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box:hover .eg-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background_hover',
			[
				'label' => __( 'Background Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view!' => '',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-stacked .eg-icon-box:hover .eg-icon' => 'background-color: {{VALUE}};',
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box:hover .eg-icon' => 'background-color: {{VALUE}};',

				],
			]
		);

		$this->add_control(
			'border_color_hover',
			[
				'label' => __( 'Border Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'icon_view' => 'framed',
				],
				'selectors' => [
					'{{WRAPPER}}.eg-icon-box--icon-view-framed .eg-icon-box:hover .eg-icon-wrap .eg-icon' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	protected function register_design_content_section_controls() {
		$this->start_controls_section(
			'section_design_content',
			[
				'label' => __( 'Title', 'elementor-gemcrypto' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eg-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Color Hover', 'elementor-gemcrypto' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'title_link[url]!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eg-title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_title',
				'default' => '',
				'selector' => '{{WRAPPER}} .eg-title',
			]
		);


		$this->end_controls_section();
	}

	protected function register_controls() {
		$this->register_layout_section_controls();

		$this->register_design_layout_section_controls();
		$this->register_design_icon_section_controls();
		$this->register_design_content_section_controls();
	}

	public function render_element_header() {

		$this->add_render_attribute( 'wrapper', 'class', 'eg-icon-box' );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
	}

	public function render_element_footer() {
		?>
			</div>
		<?php
	}

	public function render_icon() {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['icon'] ) && ! Icons_Manager::is_migration_allowed() ) {
			// add old default
			$settings['icon'] = 'fa fa-star';
		}

		if ( ! empty( $settings['icon'] ) ) {
			$this->add_render_attribute( 'icon', 'class', $settings['icon'] );
			$this->add_render_attribute( 'icon', 'aria-hidden', 'true' );
		}

		$migrated = isset( $settings['__fa4_migrated']['select_icon'] );
		$is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

		if ( $is_new || $migrated ) {
			Icons_Manager::render_icon( $settings['select_icon'], [ 'aria-hidden' => 'true' ] );
		} else {
			?>
				<i <?php $this->print_render_attribute_string( 'icon' ); ?>></i>
			<?php
		}
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$target = $settings['title_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['title_link']['nofollow'] ? ' rel="nofollow"' : '';
		$custom_attributes = explode("|",$settings['title_link']['custom_attributes']);
		$data_attr_link = (!empty($custom_attributes[0]) && count($custom_attributes) > 1 ) ? 'data-'.$custom_attributes[0].'='.$custom_attributes[1] : ( !empty($custom_attributes[0]) ? 'data-'.$custom_attributes[0] : '' ) ;

		$this->render_element_header();

		?>

		<div class="eg-icon-wrap">
			<?php if (!empty($settings['title_link']['url'])): ?>
				<a class="eg-icon" href="<?php echo esc_url($settings['title_link']['url']) ; ?>" <?php echo $target.$nofollow.$data_attr_link; ?>>
					<?php $this->render_icon(); ?>
				</a>
			<?php else: ?>
				<span class="eg-icon">
					<?php $this->render_icon(); ?>
				</span>
			<?php endif; ?>
		</div>
		<div class="eg-content">
			<?php
			if( !empty($settings['title_link']['url']) ) {
				echo '<h3 class="eg-title">'.
							'<a href="' .  esc_url($settings['title_link']['url']) . '" '.$target.$nofollow.$data_attr_link.'>' . $settings['title'] . '</a>' .
						'</h3>';
			}else {
				echo '<h3 class="eg-title">'.
							'<span>' . $settings['title'] . '</span>' .
						'</h3>';
			}
			?>
		</div>

		<?php

		$this->render_element_footer();

	}

	protected function content_template() {

	}
}
