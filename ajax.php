<?php
if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


class Ajax_Manager
{
    function __construct()
    {
        // # Ajax: use as action in $.ajax()

        add_action('wp_ajax_post_load_more', array($this, 'post_load_more'));
        add_action('wp_ajax_nopriv_post_load_more', array($this, 'post_load_more'));


    }

    public function post_load_more()
    {

      $current_paged = isset($_POST['current_page']) ? $_POST['current_page'] + 1 : max(1, get_query_var('paged')) ;

      $show_thumbnail = (isset($_POST["show_thumbnail"])) ? $_POST["show_thumbnail"] : '';
      $show_title = (isset($_POST["show_title"])) ? $_POST["show_title"] : '';
      $show_date = (isset($_POST["show_date"])) ? $_POST["show_date"] : '';
      $show_excerpt = (isset($_POST["show_excerpt"])) ? $_POST["show_excerpt"] : '';
      $show_read_more = (isset($_POST["show_read_more"])) ? $_POST["show_read_more"] : '';
      $read_more_text = (isset($_POST["read_more_text"])) ? $_POST["read_more_text"] : 'Load More';
      $posts_per_page = isset($_POST['posts_per_page'])? $_POST['posts_per_page'] : 3;
      $tags = isset($_POST['tags'])? $_POST['tags'] : '';
      $categories = isset($_POST['categories'])? $_POST['categories'] : '';
      $orderby = isset($_POST['orderby'])? $_POST['orderby'] : 'date';
      $order = isset($_POST['order'])? $_POST['order'] : 'desc';

      $args = array(
        'post_type' => 'post',
        'posts_per_page' => $posts_per_page,
        'paged' => $current_paged,
        'tags' => $tags,
        'categories' => $categories,
        'orderby' => $orderby,
        'order' => $order,
      );

      $query = new \WP_Query( $args );
      if ( $query->have_posts() ) :
         while ( $query->have_posts() ) : $query->the_post();

         ?>
         <div class="eg-post-item post-<?php echo  get_the_ID(); ?>">
           <?php if ( $show_thumbnail != '' && get_the_post_thumbnail(get_the_ID()) != '' ): ?>
             <a class="eg-post__thumbnail__link" href="<?php echo get_the_permalink(); ?>">
               <div class="eg-post__thumbnail">
                   <?php echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' ); ?>
               </div>
             </a>
           <?php endif; ?>
           <div class="eg-post__text">
             <?php if ( $show_title != ''): ?>
               <h3 class="eg-post__title">
                 <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
               </h3>
             <?php endif; ?>
             <?php if ( $show_date != '' ): ?>
               <div class="eg-post__meta">
                 <span class="eg-post__date"><?php echo get_the_date(); ?></span>
               </div>
             <?php endif; ?>
             <?php if ( $show_excerpt != '' ): ?>
               <div class="eg-post__excerpt">
                 <p><?php echo get_the_excerpt(); ?></p>
               </div>
             <?php endif; ?>
             <?php if ( $show_read_more != '' ): ?>
               <a class="eg-post__read-more" href="<?php echo get_the_permalink(); ?>"><?php echo $read_more_text; ?></a>
             <?php endif; ?>
           </div>
         </div>
         <?php
         endwhile;
       endif;
       wp_reset_postdata();

       exit();
    }

}

$Ajax_Manager =  new Ajax_Manager();
