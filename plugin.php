<?php
namespace ElementorGemcrypto;

/**
 * Class Plugin
 *
 * Main Plugin class
 * @since 1.0.0
 */
class Plugin {

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @var Plugin The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return Plugin An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public $widgets = array();

	public function widgets_list() {

		$this->widgets = array(
			'eg-icon-box',
			'eg-icon-text',
			'eg-icon-list',
			'eg-button-copy',
			'eg-relaunch-plan',
			'eg-members',
			'eg-members-carousel',
			'eg-partners',
			'eg-partners-carousel',
			'eg-step-list',
			'eg-posts',
			'eg-posts-carousel',
			'eg-reviews',
			'eg-reviews-carousel',
			'eg-language-switcher',
			'eg-apps',
			'eg-phases',
			'eg-phases-carousel',
			'eg-scrolling-list',
			'eg-projects',
			'eg-projects-carousel',
			'eg-testimonials',
			'eg-testimonials-carousel',
			'eg-account-link',
		);

		return $this->widgets;
	}

	/**
	 * Register styles
	 */
	public function register_styles() {
		wp_register_style( 'eg-show-more', plugins_url( '/assets/css/show-more.css', __FILE__ ) );
		wp_register_style( 'eg-widget-carousel', plugins_url( '/assets/css/widget-carousel.css', __FILE__ ) );

		wp_register_style( 'eg-icon-box', plugins_url( '/assets/css/icon-box.css', __FILE__ ) );
		wp_register_style( 'eg-icon-text', plugins_url( '/assets/css/icon-text.css', __FILE__ ) );
		wp_register_style( 'eg-icon-list', plugins_url( '/assets/css/icon-list.css', __FILE__ ) );
		wp_register_style( 'eg-button-copy', plugins_url( '/assets/css/button-copy.css', __FILE__ ) );
		wp_register_style( 'eg-relaunch-plan', plugins_url( '/assets/css/relaunch-plan.css', __FILE__ ) );
		wp_register_style( 'eg-members', plugins_url( '/assets/css/members.css', __FILE__ ) );
		wp_register_style( 'eg-partners', plugins_url( '/assets/css/partners.css', __FILE__ ) );

		wp_register_style( 'eg-step-list', plugins_url( '/assets/css/step-list.css', __FILE__ ) );
		wp_register_style( 'eg-posts', plugins_url( '/assets/css/posts.css', __FILE__ ) );

		wp_register_style( 'eg-reviews', plugins_url( '/assets/css/reviews.css', __FILE__ ) );
		wp_register_style( 'eg-language-switcher', plugins_url( '/assets/css/language-switcher.css', __FILE__ ) );
		wp_register_style( 'eg-apps', plugins_url( '/assets/css/apps.css', __FILE__ ) );
		wp_register_style( 'eg-phases', plugins_url( '/assets/css/phases.css', __FILE__ ) );
		wp_register_style( 'eg-scrolling-list', plugins_url( '/assets/css/scrolling-list.css', __FILE__ ) );

		wp_register_style( 'eg-projects', plugins_url( '/assets/css/projects.css', __FILE__ ) );
		wp_register_style( 'eg-testimonials', plugins_url( '/assets/css/testimonials.css', __FILE__ ) );
		wp_register_style( 'eg-account-link', plugins_url( '/assets/css/account-link.css', __FILE__ ) );

	}

	/**
	 * Enqueue styles
	 */
	public function enqueue_styles() {

		wp_enqueue_style( 'eg-elements', plugins_url( '/assets/css/elements.css', __FILE__ ) );
	}

	/**
	 * Register scripts
	 */
	public function register_scripts() {
		wp_register_script( 'eg-swiper', plugins_url( '/assets/lib/swiper/swiper.min.js', __FILE__ ), [ 'jquery' ], false, true );
		wp_register_script( 'eg-marquee', plugins_url( '/assets/lib/marquee/jquery.marquee.min.js', __FILE__ ), [ 'jquery' ], false, true );

		wp_register_script( 'eg-show-more', plugins_url( '/assets/js/show-more.js', __FILE__ ), [ 'jquery' ], false, true );
		wp_register_script( 'eg-widget-carousel', plugins_url( '/assets/js/widget-carousel.js', __FILE__ ), [ 'jquery' ], false, true );
		wp_register_script( 'eg-scrolling-list', plugins_url( '/assets/js/scrolling-list.js', __FILE__ ), [ 'jquery' ], false, true );
		wp_register_script( 'eg-members', plugins_url( '/assets/js/members.js', __FILE__ ), [ 'jquery' ], false, true );


		wp_register_script( 'eg-button-copy', plugins_url( '/assets/js/button-copy.js', __FILE__ ), [ 'jquery' ], false, true );
		wp_register_script( 'eg-language-switcher', plugins_url( '/assets/js/language-switcher.js', __FILE__ ), [ 'jquery' ], false, true );

		/*wp_register_script( 'eg-ajax', plugins_url( '/assets/js/ajax.js', __FILE__ ), [ 'jquery' ], false, true );

		wp_localize_script('eg-ajax', 'ajax_widget_vars', [
				'ajax_url' => admin_url('admin-ajax.php'),
				'ajax_post_load_more' => 'post_load_more',
		]);*/

	}

	/**
	 * Enqueue scripts
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( 'eg-elements', plugins_url( '/assets/js/elements.js', __FILE__ ), [ 'jquery' ], false, true );

	}

	/**
	 * Include Widgets files
	 *
	 * Load widgets files
	 */
	private function include_widgets_files() {

		foreach( $this->widgets_list() as $widget ) {
			require_once( __DIR__ . '/widgets/'. $widget .'/widget.php' );

			foreach( glob( __DIR__ . '/widgets/'. $widget .'/skins/*.php') as $filepath ) {
				include $filepath;
			}
		}

	}

	/**
	 * Register Category
	 *
	 * Register new Elementor category.
	 */
	public function add_category( $elements_manager ) {
		$elements_manager->add_category(
			'elementor-gemcrypto',
			[
				'title' => esc_html__( 'Elementor Gemcrypto', 'elementor-gemcrypto' )
			]
		);
	}

	/**
	 * Register Widgets
	 *
	 * Register new Elementor widgets.
	 */
	public function register_widgets() {
		// Its is now safe to include Widgets files
		$this->include_widgets_files();

		// Register Widgets
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Icon_Box\EG_Icon_Box() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Icon_Text\EG_Icon_Text() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Icon_List\EG_Icon_List() );

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Button_Copy\EG_Button_Copy() );

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Relaunch_Plan\EG_Relaunch_Plan() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Members\EG_Members() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Members_Carousel\EG_Members_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Partners\EG_Partners() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Partners_Carousel\EG_Partners_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Step_List\EG_Step_List() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Posts\EG_Posts() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Posts_Carousel\EG_Posts_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Reviews\EG_Reviews() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Reviews_Carousel\EG_Reviews_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Language_Switcher\EG_Language_Switcher() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Apps\EG_Apps() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Phases\EG_Phases() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Phases_Carousel\EG_Phases_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Scrolling_List\EG_Scrolling_List() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Projects\EG_Projects() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Projects_Carousel\EG_Projects_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Testimonials\EG_Testimonials() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Testimonials_Carousel\EG_Testimonials_Carousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Account_Link\EG_Account_Link() );

	}

	/**
	 *  Plugin class constructor
	 *
	 * Register plugin action hooks and filters
	 */
	public function __construct() {
		add_action( 'elementor/editor/before_enqueue_scripts', function() {
		   wp_enqueue_style( 'eg-editor', plugins_url( '/assets/css/editor.css', __FILE__ ) );
		});

		// Widget styles
		add_action( 'elementor/frontend/after_register_styles', [ $this, 'register_styles' ] );
		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'enqueue_styles' ] );

		// Widget scripts
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'register_scripts' ] );
		add_action( 'elementor/frontend/before_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		// Register category
		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );

		// Register widgets
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );

	}
}

// Instantiate Plugin Class
Plugin::instance();
