( function( $ ) {

  var SelectDropDown = function ( $scope , $) {
    var idWidget = $scope.data('id');
    var selectedBox = $scope.find(".elementor-widget-container");
    var dropDown = $scope.find(".eg-select-list");
    var dropDownOption = $scope.find(".eg-select-list .eg-select-option");
    var btnSelect = $scope.find(".eg-select-active");
    var selectActive = $scope.find(".eg-select-active .eg-selected");

    var selectedIndex = localStorage.getItem('selected-index-'+idWidget) || '';

    dropDownOption.each(function (index,e) {
      if ( $(this).data('index') == selectedIndex ) {
        let content = $(this).find('.eg-option').clone();
        $(this).addClass('selected');
        selectActive.html(content);
      }
    });

    btnSelect.on("click", function(e){
      $(this).find('.eg-arrow').toggleClass('down');
      dropDown.slideToggle();
    });

    dropDown.on("click", ".eg-select-option", function(e){
      let content = $(this).find('.eg-option').clone();
      dropDown.children('.eg-select-option').removeClass('selected');
      $(this).addClass('selected');
      localStorage.setItem('selected-index-'+idWidget, $(this).data('index') );
      selectActive.html(content);
      dropDown.slideUp();
      btnSelect.find('.eg-arrow').toggleClass('down');
    });

    var resizeObserver = new ResizeObserver(() => {
      let lastHeight  = selectedBox.outerHeight();
      dropDown.css('top', lastHeight + 'px' );
    });
    resizeObserver.observe(selectedBox[0]);

    $(document).on('click', function (e) {
      if(!$scope.is(e.target) && $scope.has(e.target).length === 0 ||
      !$scope.find(btnSelect).is(e.target) && $scope.find(btnSelect).has(e.target).length === 0 ){
        dropDown.slideUp();
        btnSelect.find('.eg-arrow').removeClass('down');
      }
    });

  }


  $( window ).on( 'elementor/frontend/init', function() {

    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-language-switcher.default', SelectDropDown );

  } );

} )( jQuery );
