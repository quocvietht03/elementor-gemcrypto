( function( $ ) {
	/**
 	 * @param $scope The Widget wrapper element as a jQuery element
	 * @param $ The jQuery alias
	 */
	var PostLoadMoreHandler = function( $scope, $ ) {
		// console.log($scope);
    var button_load_more = $scope.find('.eg-pagination .btn__load-more');
    var box = $scope.find('.eg-posts');
    button_load_more.on('click', function(event) {
        event.preventDefault();
        let settings = $(this).data('load-more');
        let data = {
          'action': ajax_widget_vars.ajax_post_load_more,
          'current_page' : settings.current_page,
          'show_thumbnail' : settings.show_thumbnail,
          'show_title' : settings.show_title,
          'show_date' : settings.show_date,
          'show_excerpt' : settings.show_excerpt,
          'show_read_more' : settings.show_read_more,
          'read_more_text' : settings.read_more_text,
					'posts_per_page' : settings.posts_per_page,
					'tags' : settings.tags,
					'categories' : settings.categories,
					'orderby' : settings.orderby,
					'order' : settings.order,
        }

        $.ajax({
            url: ajax_widget_vars.ajax_url,
            type: 'POST',
            data: data,
            beforeSend : function ( xhr ) {
              // button_load_more.text('Loading...');
            },
            success: function (response) {

                if (response) {
                  box.append(response);
                  if ( settings.current_page < settings.max_num_pages) {
                    settings.current_page++;
                  }
                  if (settings.current_page == settings.max_num_pages) {
                    button_load_more.remove();
                  }
                }else {
                  button_load_more.remove();
                  console.log('error');
                }
            },
        });
    });

 	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {

		elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-posts.default', PostLoadMoreHandler );

	} );

} )( jQuery );
