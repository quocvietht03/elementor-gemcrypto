( function( $ ) {
	/**
 	 * @param $scope The Widget wrapper element as a jQuery element
	 * @param $ The jQuery alias
	 */
	var MembersHandler = function( $scope, $ ) {
    var itemMember = $scope.find('.eg-member');
    var resizeObserverSkinQingShui = new ResizeObserver(() => {
      itemMember.each(function (index) {
          let itemMemberId = $scope.find('.item_'+ $(this).data('item-id'));
          let thumbnailHeight  = itemMemberId.find('.eg-thumbnail').outerHeight();
          let conatctHeight  = itemMemberId.find('.eg-contact').outerHeight();
          let socialHeight  = itemMemberId.find('.eg-social').outerHeight();
          let offetConact = thumbnailHeight - 50 - conatctHeight;
          let offetSocial = thumbnailHeight - 50 - socialHeight;

          itemMemberId.find('.eg-contact').css('top', offetConact + 'px' );
          itemMemberId.find('.eg-social').css('top', offetSocial + 'px' );
      });

    });
    resizeObserverSkinQingShui.observe(itemMember[0]);


 	};


	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {

		elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members.eg-members-skin-qingshui', MembersHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.eg-members-carousel-skin-qingshui', MembersHandler );

	} );

} )( jQuery );
