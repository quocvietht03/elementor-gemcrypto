( function( $ ) {
	/**
 	 * @param $scope The Widget wrapper element as a jQuery element
	 * @param $ The jQuery alias
	 */
	var ScrollingHandler = function( $scope, $ ) {
		let marquee = $scope.find('.eg-scroll-text');
		let marquee_data = $scope.find('.eg-scrolling-list').data('marquee');
		marquee.marquee( marquee_data );
 	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {

		elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-scrolling-list.default', ScrollingHandler );

	} );

} )( jQuery );
