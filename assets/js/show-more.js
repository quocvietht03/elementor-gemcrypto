( function( $ ) {

  var ShowMoreHandler = function( $scope, $ ) {
  		//console.log($scope);

      if(0 === $scope.find('.has-show-more').length) {
        return;
      }

      var $item = $scope.find('.elementor-item'),
          $initShow  = $scope.find('.eg-show-more').data('show'),
          $initLoad  = $scope.find('.eg-show-more').data('load'),
  				$showMoreBtn  = $scope.find('.eg-show-more-btn'),
          $showLessBtn  = $scope.find('.eg-show-less-btn');

      $item.slice(0, $initShow).show();

      $showMoreBtn.on('click', function (event) {
        event.preventDefault();

        $scope.find('.elementor-item:hidden').slice(0, $initLoad).slideDown();
        if ($scope.find('.elementor-item:hidden').length == 0) {
            $showMoreBtn.hide();
            $showLessBtn.show();
        }
      });

      $showLessBtn.on('click', function (event) {
        event.preventDefault();

        $showLessBtn.hide();
        $showMoreBtn.show();
        $item.slice($initShow).slideUp();
      });

  };

  $( window ).on( 'elementor/frontend/init', function() {
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-partners.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-reviews.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-reviews.skin-cards', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-apps.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-phases.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-projects.default', ShowMoreHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-testimonials.default', ShowMoreHandler );

  });

} )( jQuery );
