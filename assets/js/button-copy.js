( function( $ ) {
	/**
 	 * @param $scope The Widget wrapper element as a jQuery element
	 * @param $ The jQuery alias
	 */


  var ButtonCopyHandler = function ( $scope ) {

    jQuery('.eg-button-copy').on('click mouseout', function (event) {
      event.preventDefault();
      let tooltip = jQuery(this).children('.tooltiptext');
      let copyText = jQuery(this).data('copy');
      if (event.type == 'click') {
        let textArea = document.createElement("textarea");
        textArea.value = copyText;
        document.body.appendChild(textArea);
        textArea.select();
        try {
          let successful = document.execCommand('copy');
        } catch(err) {
          console.log(err);
        }
        textArea.remove();
        tooltip.text("Copied");
      }else {
        tooltip.text('Copy');
      }

    });

  }


  $( window ).on( 'elementor/frontend/init', function() {

		elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-button-copy.default', ButtonCopyHandler );

	} );


} )( jQuery );
