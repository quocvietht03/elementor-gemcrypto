( function( $ ) {

  var SwiperSliderHandler = function( $scope, $ ) {
  		//console.log($scope);

  		var $selector = $scope.find('.swiper-container'),
  				$dataSwiper  = $selector.data('swiper'),
  				newSwiper = new Swiper($selector, $dataSwiper);

  };

  $( window ).on( 'elementor/frontend/init', function() {
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-partners-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-reviews-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-reviews-carousel.skin-cards', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-posts-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-posts-carousel.skin-qingshui', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-posts-carousel.skin-duge', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-posts-carousel.skin-puli', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.skin-overlay', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.skin-qingshui', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.skin-puli', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-members-carousel.skin-duge', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-phases-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-projects-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-projects-carousel.skin-sidu', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-testimonials-carousel.default', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-testimonials-carousel.skin-horizontal', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-testimonials-carousel.skin-sidu', SwiperSliderHandler );
    elementorFrontend.hooks.addAction( 'frontend/element_ready/eg-testimonials-carousel.skin-yachi', SwiperSliderHandler );

  });

} )( jQuery );
